#include <config.h>

#define _COPYRIGHT_STRING \
  "Copyright (C) 2014 Barry Schwartz"

// Copyright (C) 2014 Barry Schwartz
//
// This file is part of Geometric-algebras.
// 
// Geometric-algebras is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
// 
// Geometric-algebras is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with Geometric-algebras.  If not, see
// <http://www.gnu.org/licenses/>.

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <libguile.h>

#include "getopt.h"
#include "progname.h"
#include <geometric-algebras/geomalgebras-dirlayout.h>

static const unsigned char guiledir_in_utf8[] = GEOMALGEBRAS_GUILEDIR_IN_UTF8;
static const unsigned char guileobjdir_in_utf8[] =
    GEOMALGEBRAS_GUILEOBJDIR_IN_UTF8;

#define _VERSION_FORMAT                                                 \
  "%s (%s) %s\n"                                                        \
  _COPYRIGHT_STRING "\n\n"                                              \
  "License GPLv3+: GNU GPL 3 or later <http://gnu.org/licenses/gpl.html>.\n" \
  "This is free software: you are free to change and redistribute it.\n" \
  "There is NO WARRANTY, to the extent permitted by law.\n\n--\n\n"

#define _HELP_FORMAT                                                    \
  "Usage: %s [OPTION] [-- GUILE ARGUMENTS]\n"                           \
  "Run Guile with Geometric-algebras lt_dlopened.\n"			\
  "\n"                                                                  \
  "  --help     display this help and exit\n"                           \
  "  --version  output version information and exit\n"                  \
  "\n"                                                                  \
  "For information about Guile arguments and usage try `guile --help'\n" \
  "\n"                                                                  \
  "Examples:\n"                                                         \
  "\n"                                                                  \
  "  %s -- -c '(display \"Hello, world.\\n\")'\n"                       \
  "\n"                                                                  \
  "  GUILE='%s --' guild compile file.scm\n"                            \
  "\n"                                                                  \
  "  alias guile='%s --'\n"                                             \
  "\n"                                                                  \
  "Report %s bugs to %s\n"                                              \
  "%s home page: <%s>\n"

#define _TRY_FOR_MORE_INFO_FORMAT "Try '%s --help' for more information.\n"

#define _UNEXPECTED_ARGUMENTS_FORMAT "Unexpected arguments:"

static int want_help = false;
static int want_version = false;

static struct option const long_options[] = {
  {"help", no_argument, &want_help, true},
  {"version", no_argument, &want_version, true},
  {NULL, 0, NULL, 0}
};

// FIXME: Our directories are added at the end to avoid interfering
// with settings of GUILE_LOAD_PATH, GUILE_LOAD_COMPILED_PATH,
// etc. However, it would be better to have our directories come
// before Guile’s built-in directories.
static void
append_to_path (const char *module, const char *path_variable, const char *dir)
{
  SCM load_path = scm_c_public_lookup (module, path_variable);
  SCM lst = scm_append (scm_list_2 (scm_variable_ref (load_path),
                                    scm_list_1 (scm_from_utf8_string (dir))));
  scm_variable_set_x (load_path, lst);
}

static void
append_to_load_path (const char *dir)
{
  append_to_path ("guile", "%load-path", dir);
}

static void
append_to_load_compiled_path (const char *dir)
{
  append_to_path ("guile", "%load-compiled-path", dir);
}

static void
main_func (void *data, int argc, char **argv)
{
  append_to_load_path ((const char *) guiledir_in_utf8);
  append_to_load_compiled_path ((const char *) guileobjdir_in_utf8);
  scm_shell (argc, argv);
}

static void
show_how_to_get_more_info ()
{
  fprintf (stderr, _TRY_FOR_MORE_INFO_FORMAT, program_name);
}

static void
show_version (void)
{
  printf (_VERSION_FORMAT, PACKAGE, PACKAGE_NAME, PACKAGE_VERSION);
  fflush (stdout);
  int argc1 = 2;
  char *argv1[] = { "guile", "--version" };
  int phony_data;
  scm_boot_guile (argc1, argv1, main_func, &phony_data);
}

static void
show_help (void)
{
  printf (_HELP_FORMAT, program_name, PACKAGE, PACKAGE, PACKAGE, PACKAGE,
          "<" PACKAGE_BUGREPORT ">", PACKAGE_NAME, PACKAGE_URL);
}

static void
unexpected_arguments (int argc, char **argv, int i)
{
  fprintf (stderr, _UNEXPECTED_ARGUMENTS_FORMAT);
  for (int j = i; j < argc; j++)
    fprintf (stderr, " %s", argv[j]);
  fprintf (stderr, "\n");
  fprintf (stderr, _TRY_FOR_MORE_INFO_FORMAT, program_name);
}

int
main (int argc, char **argv)
{
  set_program_name (argv[0]);

  int exit_status = 0;

  int option_index;
  int c = getopt_long (argc, argv, "", long_options, &option_index);
  while (c != -1 && c != '?')
    c = getopt_long (argc, argv, "", long_options, &option_index);

  if (c == '?')
    {
      show_how_to_get_more_info ();
      exit_status = 1;
    }
  else if (want_version)
    show_version ();
  else if (want_help)
    show_help ();
  else if (argc != 1 && strcmp (argv[optind - 1], "--") != 0)
    {
      unexpected_arguments (argc, argv, optind);
      exit_status = 1;
    }
  else
    {
      // Build a new command line to pass to the Guile shell.
      int argc1 = argc - optind + 1;
      char *argv1[argc1];
      argv1[0] = "guile";
      for (int i = optind; i < argc; i++)
        argv1[i - optind + 1] = argv[i];

      int phony_data;
      scm_boot_guile (argc1, argv1, main_func, &phony_data);
    }
  return exit_status;
}
