// -*- mode: c++; c-file-style: "gnu" -*-

// Copyright (C) 2014 Barry Schwartz
//
// This file is part of Geometric-algebras.
// 
// Geometric-algebras is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 3 of the
// License, or (at your option) any later version.
// 
// Geometric-algebras is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include "config.h"

#include <string>
#include <cstring>
#include <sstream>
#include <stdexcept>
#include <stdlib.h>
#include <string.h>

#include <ginac/ginac.h>
#include <libguile.h>

using namespace GiNaC;

static const char ginac_module[] = "ga-generator ginac-interface";

//-------------------------------------

static void
throw_cxx_exception (const char *subr, std::exception &excep)
{
  scm_misc_error (subr, excep.what (), SCM_EOL);
}

//-------------------------------------

static void
SCM__destroy_symbol (void *p)
{
  delete (symbol *) p;
}

static SCM
make_symb (symbol *symb)
{
  SCM proc = scm_c_private_ref (ginac_module, "GiNaC:make-symbol^");
  return
    scm_call_1 (proc, scm_from_pointer (symb, SCM__destroy_symbol));
}

static SCM
SCM__create_symbol (SCM str)
{
  scm_dynwind_begin ((scm_t_dynwind_flags) 0);
  char *s = scm_to_utf8_stringn (str, NULL);
  scm_dynwind_free ((void *) s);
  symbol *symb;  
  try
    {
      symb = new symbol (s);
    }
  catch (std::exception &excep)
    {
      throw_cxx_exception ("GiNaC:make-symbol", excep);
    }
  scm_dynwind_end ();
  return make_symb (symb);
}

static symbol *
get_symb (SCM symb_obj)
{
  SCM proc = scm_c_private_ref (ginac_module, "GiNaC:symbol-pointer^");
  return (symbol *) scm_to_pointer (scm_call_1 (proc, symb_obj));
}

static SCM
SCM__symbol_name (SCM symb_obj)
{
  symbol *symb = get_symb (symb_obj);
  const char *name;
  try
    {
      name = symb->get_name ().c_str ();
    }
  catch (std::exception &excep)
    {
      throw_cxx_exception ("GiNaC:symbol-name", excep);
    }  
  return scm_from_utf8_string (name);
}

//-------------------------------------

static void
SCM__destroy_numeric (void *p)
{
  delete (numeric *) p;
}

static SCM
make_numeric (numeric *n)
{
  SCM proc = scm_c_private_ref (ginac_module, "GiNaC:make-numeric^");
  return
    scm_call_1 (proc, scm_from_pointer (n, SCM__destroy_numeric));
}

static SCM
string_to_numeric (SCM str)
{
  scm_dynwind_begin ((scm_t_dynwind_flags) 0);
  char *s = scm_to_utf8_stringn (str, NULL);
  scm_dynwind_free ((void *) s);
  numeric *numeric_val;
  try
    {
      numeric_val = new numeric (s);
    }
  catch (std::exception &excep)
    {
      throw_cxx_exception ("GiNaC:make-numeric", excep);
    }
  scm_dynwind_end ();
  return make_numeric (numeric_val);
}

static SCM
number_to_numeric (SCM n)
{
  return
    string_to_numeric (scm_number_to_string (n, scm_from_int (10)));
}

static SCM
exact_integer_to_numeric (SCM n)
{
  SCM result;
  if (scm_is_signed_integer (n, LONG_MIN, LONG_MAX))
    {
      numeric *numeric_val;
      try
	{
	  numeric_val = new numeric (scm_to_long (n));
	}
      catch (std::exception &excep)
	{
	  throw_cxx_exception ("GiNaC:make-numeric", excep);
	}      
      result = make_numeric (numeric_val);
    }
  else if (scm_is_unsigned_integer (n, 0, ULONG_MAX))
    {
      numeric *numeric_val;
      try
	{
	  numeric_val = new numeric (scm_to_ulong (n));
	}
      catch (std::exception &excep)
	{
	  throw_cxx_exception ("GiNaC:make-numeric", excep);
	}      
      result = make_numeric (numeric_val);
    }
  else
    result = number_to_numeric (n);
  return result;
}

static SCM
exact_rational_to_numeric (SCM n)
{
  SCM result;
  SCM numer = scm_numerator (n);
  SCM denom = scm_denominator (n);
  if (scm_is_signed_integer (numer, LONG_MIN, LONG_MAX)
      && scm_is_signed_integer (denom, LONG_MIN, LONG_MAX))
    {
      numeric *numeric_val;
      try
	{
	  numeric_val = new numeric (scm_to_long (numer),
				     scm_to_long (denom));
	}
      catch (std::exception &excep)
	{
	  throw_cxx_exception ("GiNaC:make-numeric", excep);
	}      
      result = make_numeric (numeric_val);
    }
  else
    result = number_to_numeric (n);
  return result;
}

static SCM
double_to_numeric (SCM n)
{
  numeric *numeric_val;
  try
    {
      numeric_val = new numeric (scm_to_double (n));
    }
  catch (std::exception &excep)
    {
      throw_cxx_exception ("GiNaC:make-numeric", excep);
    }      
  return make_numeric (numeric_val);
}

static SCM
SCM__scm_to_ginac_numeric (SCM n)
{
  SCM result;
  if (scm_is_string (n))
    result = string_to_numeric (n);
  else if (scm_is_real (n)) 
    {
      if (scm_is_exact_integer (n))
	result = exact_integer_to_numeric (n);
      else if (scm_is_exact (n))
	result = exact_rational_to_numeric (n);
      else
	result = double_to_numeric (n);
    }
  else
    {
      SCM_ASSERT_TYPE (scm_is_number (n), n, SCM_ARG1,
		       "GiNaC:make-numeric", "number or string");

      // FIXME: This could be made much more efficient. (Keep in
      // mind, we would like to write it in a way that can handle
      // exact complex numbers, in case Guile ever gets them.)
      SCM s = scm_number_to_string (n, scm_from_int (10));
      const size_t len = scm_c_string_length (s);
      SCM UNICODE_i = scm_from_uint (0x69);
      if (scm_char_to_integer (scm_c_string_ref (s, len - 1))
	  == UNICODE_i)
	{
	  SCM ginac_notation =
	    scm_string_append
	    (scm_list_2
	     (scm_string_drop_right (s, scm_from_int (1)),
	      scm_from_utf8_string ("*I")));
	  result = string_to_numeric (ginac_notation);
	}
      else
	result = string_to_numeric (s);
    }
  return result;
}

static numeric *
get_numeric (SCM numeric_obj)
{
  SCM proc = scm_c_private_ref (ginac_module, "GiNaC:numeric-pointer^");
  return (numeric *) scm_to_pointer (scm_call_1 (proc, numeric_obj));
}

static SCM
SCM__numeric_str (SCM numeric_obj)
{
  numeric *num = get_numeric (numeric_obj);
  scm_dynwind_begin ((scm_t_dynwind_flags) 0);
  char *str = NULL;
  scm_dynwind_free ((void *) str);
  try
    {
      std::stringstream *ss = new std::stringstream;
      *ss << *num;
      std::string *s = new std::string (ss->str ());
      str = (char *) calloc (s->length () + 1, sizeof (char));
      memcpy ((void *) str, (void *) s->c_str (),
	      (s->length () + 1) * sizeof (char));
      delete s;
      delete ss;
    }
  catch (std::exception &excep)
    {
      throw_cxx_exception ("GiNaC:numeric->string", excep);
    }      
  SCM result = scm_from_utf8_string (str);
  scm_dynwind_end ();
  return result;
}

static SCM
SCM__numeric_digits ()
{
  return scm_from_long ((long) Digits);
}

static SCM
SCM__set_numeric_digits (SCM n)
{
  try
    {
      Digits = scm_to_long (n);
    }
  catch (std::exception &excep)
    {
      throw_cxx_exception ("GiNaC:Digits", excep);
    }  
  return n;
}

//-------------------------------------

static void
SCM__destroy_ex (void *p)
{
  delete (ex *) p;
}

static SCM
make_ex (ex *n)
{
  SCM proc = scm_c_private_ref (ginac_module, "GiNaC:make-ex^");
  return
    scm_call_1 (proc, scm_from_pointer (n, SCM__destroy_ex));
}

static SCM
SCM__make_ex ()
{
  ex *e;
  try
    {
      e = new ex;
    }
  catch (std::exception &excep)
    {
      throw_cxx_exception ("GiNaC:make-ex", excep);
    }
  return make_ex (e);
}

static ex *
get_ex (SCM expr)
{
  SCM proc = scm_c_private_ref (ginac_module, "GiNaC:ex-pointer^");
  return (ex *) scm_to_pointer (scm_call_1 (proc, expr));
}

static SCM
SCM__ex_str (SCM expr)
{
  ex *e = get_ex (expr);
  scm_dynwind_begin ((scm_t_dynwind_flags) 0);
  char *str = NULL;
  scm_dynwind_free ((void *) str);
  try
    {
      std::stringstream *ss = new std::stringstream;
      *ss << *e;
      std::string *s = new std::string (ss->str ());
      str = (char *) calloc (s->length () + 1, sizeof (char));
      memcpy ((void *) str, (void *) s->c_str (),
	      (s->length () + 1) * sizeof (char));
      delete s;
      delete ss;
    }
  catch (std::exception &excep)
    {
      throw_cxx_exception ("GiNaC:ex->string", excep);
    }      
  SCM result = scm_from_utf8_string (str);
  scm_dynwind_end ();
  return result;
}

//-------------------------------------

static SCM
SCM__ex_nops(SCM expr)
{
  return scm_from_size_t (get_ex (expr)->nops ());
}

static SCM
SCM__ex_op (SCM expr, SCM i)
{
  ex *e = get_ex (expr);
  const size_t n = e->nops ();
  const size_t j = scm_to_size_t (i);
  const char *message = "non-negative integer less than ~a";
  if (n <= j)
    scm_out_of_range ("GiNaC:ex-op", i);
  ex *op;
  try
    {
      op = new ex (e->op (j));
    }
  catch (std::exception &excep)
    {
      throw_cxx_exception ("GiNaC:ex-op", excep);
    }
  return make_ex (op);
}

//-------------------------------------

static SCM
SCM__is_symbol (SCM expr)
{
  return scm_from_bool (is_a <symbol> (*get_ex (expr)));
}

static SCM
SCM__ex_symbol (SCM symb_obj)
{
  symbol *symb = get_symb (symb_obj);
  ex *e;
  try
    {
      e = new ex (*symb);
    }
  catch (std::exception &excep)
    {
      throw_cxx_exception ("GiNaC:symbol->ex", excep);
    }
  return make_ex (e);
}

static SCM
SCM__ex_to_symbol (SCM ex_obj)
{
  ex *e = get_ex (ex_obj);
  SCM_ASSERT_TYPE ((is_a <symbol> (*e)), ex_obj, SCM_ARG1,
		   "GiNaC:ex->symbol",
		   "GiNaC:ex that satisfies GiNaC:ex-symbol?");
  symbol *symb;
  try
    {
      symb = new symbol (ex_to <symbol> (*e));
    }
  catch (std::exception &excep)
    {
      throw_cxx_exception ("GiNaC:ex->symbol", excep);
    }
  return make_symb (symb);
}

//-------------------------------------

static SCM
SCM__is_numeric (SCM expr)
{
  return scm_from_bool (is_a <numeric> (*get_ex (expr)));
}

static SCM
SCM__ex_numeric (SCM numeric_obj)
{
  numeric *num = get_numeric (numeric_obj);
  ex *e;
  try
    {
      e = new ex (*num);
    }
  catch (std::exception &excep)
    {
      throw_cxx_exception ("GiNaC:numeric->ex", excep);
    }
  return make_ex (e);
}

static SCM
SCM__ex_to_numeric (SCM ex_obj)
{
  ex *e = get_ex (ex_obj);
  SCM_ASSERT_TYPE ((is_a <numeric> (*e)), ex_obj, SCM_ARG1,
		   "GiNaC:ex->numeric",
		   "GiNaC:ex that satisfies GiNaC:ex-numeric?");
  numeric *num;
  try
    {
      num = new numeric (ex_to <numeric> (*e));
    }
  catch (std::exception &excep)
    {
      throw_cxx_exception ("GiNaC:ex->numeric", excep);
    }
  return make_numeric (num);
}

//-------------------------------------

static SCM
SCM__is_add (SCM expr)
{
  return scm_from_bool (is_a <add> (*get_ex (expr)));
}

static SCM
SCM__ex_add (SCM ex1, SCM ex2)
{
  ex *a = get_ex (ex1);
  ex *b = get_ex (ex2);
  ex *result;
  try
    {
      result = new ex (*a + *b);
    }
  catch (std::exception &excep)
    {
      throw_cxx_exception ("GiNaC:ex-add", excep);
    }
  return make_ex (result);
}

static SCM
SCM__ex_sub (SCM ex1, SCM ex2)
{
  ex *a = get_ex (ex1);
  ex *b = get_ex (ex2);
  ex *result;
  try
    {
      result = new ex (*a - *b);
    }
  catch (std::exception &excep)
    {
      throw_cxx_exception ("GiNaC:ex-sub", excep);
    }
  return make_ex (result);
}

//-------------------------------------

static SCM
SCM__is_mul (SCM expr)
{
  return scm_from_bool (is_a <mul> (*get_ex (expr)));
}

static SCM
SCM__ex_mul (SCM ex1, SCM ex2)
{
  ex *a = get_ex (ex1);
  ex *b = get_ex (ex2);
  ex *result;
  try
    {
      result = new ex (*a * *b);
    }
  catch (std::exception &excep)
    {
      throw_cxx_exception ("GiNaC:ex-mul", excep);
    }
  return make_ex (result);
}

static SCM
SCM__ex_div (SCM ex1, SCM ex2)
{
  ex *a = get_ex (ex1);
  ex *b = get_ex (ex2);
  ex *result;
  try
    {
      result = new ex (*a / *b);
    }
  catch (std::exception &excep)
    {
      throw_cxx_exception ("GiNaC:ex-div", excep);
    }
  return make_ex (result);
}

//-------------------------------------

static SCM
SCM__is_power (SCM expr)
{
  return scm_from_bool (is_a <power> (*get_ex (expr)));
}

static SCM
SCM__ex_power (SCM ex1, SCM ex2)
{
  ex *a = get_ex (ex1);
  ex *b = get_ex (ex2);
  ex *result;
  try
    {
      result = new ex (pow (*a, *b));
    }
  catch (std::exception &excep)
    {
      throw_cxx_exception ("GiNaC:ex-power", excep);
    }
  return make_ex (result);
}

//-------------------------------------

static SCM
SCM__is_function (SCM expr)
{
  return scm_from_bool (is_a <function> (*get_ex (expr)));
}

static SCM
SCM__function_name (SCM expr)
{
  ex *e = get_ex (expr);
  SCM_ASSERT_TYPE ((is_a <function> (*e)), expr, SCM_ARG1,
		   "GiNaC:ex-function->string",
		   "GiNaC:ex that satisfies GiNaC:ex-function?");
  const char *name;
  try
    {
      name = ex_to <function> (*e).get_name ().c_str ();
    }
  catch (std::exception &excep)
    {
      throw_cxx_exception ("GiNaC:ex-function->string", excep);
    }  
  return scm_from_utf8_string (name);
}

static SCM
SCM__ex_abs (SCM ex1)
{
  ex *a = get_ex (ex1);
  ex *result;
  try
    {
      result = new ex (abs (*a));
    }
  catch (std::exception &excep)
    {
      throw_cxx_exception ("GiNaC:ex-abs", excep);
    }
  return make_ex (result);
}

//-------------------------------------

static SCM
SCM__ex_negate (SCM ex1)
{
  ex *a = get_ex (ex1);
  ex *result;
  try
    {
      result = new ex (- (*a));
    }
  catch (std::exception &excep)
    {
      throw_cxx_exception ("GiNaC:ex-negate", excep);
    }
  return make_ex (result);
}

static SCM
SCM__ex_expand (SCM ex1)
{
  ex *a = get_ex (ex1);
  ex *result;
  try
    {
      result = new ex (a->expand ());
    }
  catch (std::exception &excep)
    {
      throw_cxx_exception ("GiNaC:ex-expand", excep);
    }
  return make_ex (result);
}

//-------------------------------------

static SCM
SCM__ex_eq (SCM ex1, SCM ex2)
{
  ex *a = get_ex (ex1);
  ex *b = get_ex (ex2);
  bool result;
  try
    {
      result = (*a == *b);
    }
  catch (std::exception &excep)
    {
      throw_cxx_exception ("GiNaC:ex=?", excep);
    }
  return scm_from_bool (result);
}

static SCM
SCM__ex_ne (SCM ex1, SCM ex2)
{
  ex *a = get_ex (ex1);
  ex *b = get_ex (ex2);
  bool result;
  try
    {
      result = (*a != *b);
    }
  catch (std::exception &excep)
    {
      throw_cxx_exception ("GiNaC:ex<>?", excep);
    }
  return scm_from_bool (result);
}

static SCM
SCM__ex_lt (SCM ex1, SCM ex2)
{
  ex *a = get_ex (ex1);
  ex *b = get_ex (ex2);
  bool result;
  try
    {
      result = (*a < *b);
    }
  catch (std::exception &excep)
    {
      throw_cxx_exception ("GiNaC:ex<?", excep);
    }
  return scm_from_bool (result);
}

static SCM
SCM__ex_gt (SCM ex1, SCM ex2)
{
  ex *a = get_ex (ex1);
  ex *b = get_ex (ex2);
  bool result;
  try
    {
      result = (*a > *b);
    }
  catch (std::exception &excep)
    {
      throw_cxx_exception ("GiNaC:ex>?", excep);
    }
  return scm_from_bool (result);
}

static SCM
SCM__ex_le (SCM ex1, SCM ex2)
{
  ex *a = get_ex (ex1);
  ex *b = get_ex (ex2);
  bool result;
  try
    {
      result = (*a <= *b);
    }
  catch (std::exception &excep)
    {
      throw_cxx_exception ("GiNaC:ex<=?", excep);
    }
  return scm_from_bool (result);
}

static SCM
SCM__ex_ge (SCM ex1, SCM ex2)
{
  ex *a = get_ex (ex1);
  ex *b = get_ex (ex2);
  bool result;
  try
    {
      result = (*a >= *b);
    }
  catch (std::exception &excep)
    {
      throw_cxx_exception ("GiNaC:ex>=?", excep);
    }
  return scm_from_bool (result);
}

//-------------------------------------------------------------------------

extern "C" void init_libguile_geometric_algebras_ginac (void);

extern "C" VISIBLE void
init_libguile_geometric_algebras_ginac (void)
{
  scm_c_define_gsubr ("GiNaC:make-symbol", 1, 0, 0,
		      (scm_t_subr) SCM__create_symbol);
  scm_c_define_gsubr ("GiNaC:symbol-name", 1, 0, 0,
		      (scm_t_subr) SCM__symbol_name);
  scm_c_define_gsubr ("GiNaC:make-numeric", 1, 0, 0,
		      (scm_t_subr) SCM__scm_to_ginac_numeric);
  scm_c_define_gsubr ("GiNaC:numeric->string", 1, 0, 0,
		      (scm_t_subr) SCM__numeric_str);
  scm_c_define_gsubr ("Digits-ref", 0, 0, 0,
		      (scm_t_subr) SCM__numeric_digits);
  scm_c_define_gsubr ("Digits-set!", 1, 0, 0,
		      (scm_t_subr) SCM__set_numeric_digits);
  scm_c_define_gsubr ("GiNaC:make-ex", 0, 0, 0,
		      (scm_t_subr) SCM__make_ex);
  scm_c_define_gsubr ("GiNaC:ex->string", 1, 0, 0,
		      (scm_t_subr) SCM__ex_str);
  scm_c_define_gsubr ("GiNaC:ex-nops", 1, 0, 0,
		      (scm_t_subr) SCM__ex_nops);
  scm_c_define_gsubr ("GiNaC:ex-op", 2, 0, 0,
		      (scm_t_subr) SCM__ex_op);
  scm_c_define_gsubr ("GiNaC:ex-symbol?", 1, 0, 0,
		      (scm_t_subr) SCM__is_symbol);
  scm_c_define_gsubr ("GiNaC:symbol->ex", 1, 0, 0,
		      (scm_t_subr) SCM__ex_symbol);
  scm_c_define_gsubr ("GiNaC:ex->symbol", 1, 0, 0,
		      (scm_t_subr) SCM__ex_to_symbol);
  scm_c_define_gsubr ("GiNaC:ex-numeric?", 1, 0, 0,
		      (scm_t_subr) SCM__is_numeric);
  scm_c_define_gsubr ("GiNaC:numeric->ex", 1, 0, 0,
		      (scm_t_subr) SCM__ex_numeric);
  scm_c_define_gsubr ("GiNaC:ex->numeric", 1, 0, 0,
		      (scm_t_subr) SCM__ex_to_numeric);
  scm_c_define_gsubr ("GiNaC:ex-add?", 1, 0, 0,
		      (scm_t_subr) SCM__is_add);
  scm_c_define_gsubr ("GiNaC:ex-add", 2, 0, 0,
		      (scm_t_subr) SCM__ex_add);
  scm_c_define_gsubr ("GiNaC:ex-sub", 2, 0, 0,
		      (scm_t_subr) SCM__ex_sub);
  scm_c_define_gsubr ("GiNaC:ex-mul?", 1, 0, 0,
		      (scm_t_subr) SCM__is_mul);
  scm_c_define_gsubr ("GiNaC:ex-mul", 2, 0, 0,
		      (scm_t_subr) SCM__ex_mul);
  scm_c_define_gsubr ("GiNaC:ex-div", 2, 0, 0,
		      (scm_t_subr) SCM__ex_div);
  scm_c_define_gsubr ("GiNaC:ex-power?", 1, 0, 0,
		      (scm_t_subr) SCM__is_power);
  scm_c_define_gsubr ("GiNaC:ex-power", 2, 0, 0,
		      (scm_t_subr) SCM__ex_power);
  scm_c_define_gsubr ("GiNaC:ex-function?", 1, 0, 0,
		      (scm_t_subr) SCM__is_function);
  scm_c_define_gsubr ("GiNaC:ex-function-name", 1, 0, 0,
		      (scm_t_subr) SCM__function_name);
  scm_c_define_gsubr ("GiNaC:ex-abs", 1, 0, 0,
		      (scm_t_subr) SCM__ex_abs);
  scm_c_define_gsubr ("GiNaC:ex-negate", 1, 0, 0,
		      (scm_t_subr) SCM__ex_negate);
  scm_c_define_gsubr ("GiNaC:ex-expand", 1, 0, 0,
		      (scm_t_subr) SCM__ex_expand);
  scm_c_define_gsubr ("GiNaC:ex=?", 2, 0, 0,
		      (scm_t_subr) SCM__ex_eq);
  scm_c_define_gsubr ("GiNaC:ex<>?", 2, 0, 0,
		      (scm_t_subr) SCM__ex_ne);
  scm_c_define_gsubr ("GiNaC:ex<?", 2, 0, 0,
		      (scm_t_subr) SCM__ex_lt);
  scm_c_define_gsubr ("GiNaC:ex>?", 2, 0, 0,
		      (scm_t_subr) SCM__ex_gt);
  scm_c_define_gsubr ("GiNaC:ex<=?", 2, 0, 0,
		      (scm_t_subr) SCM__ex_le);
  scm_c_define_gsubr ("GiNaC:ex>=?", 2, 0, 0,
		      (scm_t_subr) SCM__ex_ge);
}

//-------------------------------------------------------------------------
