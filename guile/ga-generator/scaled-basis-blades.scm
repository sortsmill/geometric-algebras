;; Copyright (C) 2014 Barry Schwartz
;;
;; This file is part of Geometric-algebras.
;; 
;; Geometric-algebras is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3 of the
;; License, or (at your option) any later version.
;; 
;; Geometric-algebras is distributed in the hope that it will be
;; useful, but WITHOUT ANY WARRANTY; without even the implied warranty
;; of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (ga-generator scaled-basis-blades)

  ;;
  ;; Orthogonal basis blades represented as bitmaps. See Leo Dorst et
  ;; al., ‘Geometric Algebra for Computer Science’, Morgan Kaufmann,
  ;; 2007, ISBN: 978-0-12-374942-0 and Daniel Fontijne, ‘Efficient
  ;; implementation of geometric algebra’, thesis, 2007, ISBN-13:
  ;; 978-90-889-10-142.
  ;;
  ;; For example:
  ;;
  ;;     1 => 0
  ;;     e1 => 1
  ;;     e2 => 10
  ;;     e1⋀e2 => 11
  ;;     e1⋀e3 => 101
  ;;     e2⋀e3 => 110
  ;;     e1⋀e2⋀e3 => 111
  ;;

  (export

   ;; (make-scaled-basis-blade real-scale fixnum-bitmap) → scaled-basis-blade
   ;; (scaled-basis-blade? obj) → boolean
   ;; (scaled-basis-blade-scale scaled-basis-blade) → real
   ;; (scaled-basis-blade-scale scaled-basis-bitmap) → fixnum
   make-scaled-basis-blade
   scaled-basis-blade?
   scaled-basis-blade-scale
   scaled-basis-blade-bitmap

   ;; (scaled-basis-blade-op sbb) → non-negative-fixnum
   scaled-basis-blade-grade

   ;; (scaled-basis-blade-op sbb1 sbb2) → scaled-basis-blade
   scaled-basis-blade-op

   )

  (import (rnrs)
          (srfi srfi-9 gnu)
          (except (guile) error)
          (ice-9 format))

  (define-immutable-record-type scaled-basis-blade
    (make-scaled-basis-blade^ scale bitmap)
    scaled-basis-blade?
    (scale scaled-basis-blade-scale^
           scaled-basis-blade-with-scale^)
    (bitmap scaled-basis-blade-bitmap^
            scaled-basis-blade-with-bitmap^))

  (set-record-type-printer!
   scaled-basis-blade
   (lambda (sbb port)
     (format port "(make-scaled-basis-blade ~s #b~b)"
             (scaled-basis-blade-scale sbb)
             (scaled-basis-blade-bitmap sbb))))

  (define (make-scaled-basis-blade scale bitmap)
    "If either the @var{scale} or the @var{bitmap} argument equals
zero, return the scalar value of the @var{scale} argument. Otherwise,
return a new scaled basis blade."
    (if (or (fxzero? bitmap) (zero? scale))
        scale
        (make-scaled-basis-blade^ scale bitmap)))

  (define (scaled-basis-blade-scale sbb)
    "Return the @code{scale} field of a scaled basis blade, or, if the
argument is a scalar, return the argument itself."
    (if (number? sbb)
        sbb
        (scaled-basis-blade-scale^ sbb)))

  (define (scaled-basis-blade-with-scale sbb scale)
    "If the @var{scale} argument equals zero, return
@var{scale}. Otherwise, return the basis blade with its @code{scale}
field replaced."
    (if (zero? scale)
        scale
        (scaled-basis-blade-with-scale^ sbb scale)))

  (define (scaled-basis-blade-bitmap sbb)
    "Return the @code{bitmap} field of a scaled basis blade, or, if the
argument is a scalar, return @code{0}."
    (if (number? sbb)
        0
        (scaled-basis-blade-bitmap^ sbb)))

  (define (scaled-basis-blade-with-bitmap sbb bitmap)
    "If the @var{bitmap} argument equals zero, return the @code{scale}
field of the basis blade. Otherwise, return the basis blade with its
@code{bitmap} field replaced."
    (if (fxzero? bitmap)
        (scaled-basis-blade-scale sbb)
        (scaled-basis-blade-with-bitmap^ sbb bitmap)))

  (define (scaled-basis-blade-grade sbb)
    "Return the grade of a scaled basis blade or scalar."
    (if (number? sbb)
        0
        (fxbit-count (scaled-basis-blade-bitmap sbb))))

  (define (scaled-basis-blade-op sbb1 sbb2)
    "Return the outer product of two @code{scaled-basis-blade}. Also
handles the cases where either argument is a @code{number} and where
both arguments are @code{number}. The return value is a
@code{scaled-basis-blade} or a @code{number}."
    (define (bad-argument-types sbb1 sbb2)
      (assertion-violation
       'scaled-basis-blade-op
       "expected scaled-basis-blade or (scalar) number arguments"
       sbb1 sbb2))
    (cond
     [(scaled-basis-blade? sbb1)
      (cond
       [(scaled-basis-blade? sbb2)
        (let ([b1 (scaled-basis-blade-bitmap sbb1)]
              [b2 (scaled-basis-blade-bitmap sbb2)])
          (if (fxzero? (fxand b1 b2))
              (let ([w1 (scaled-basis-blade-scale sbb1)])
                (if (zero? w1)
                    0
                    (let ([w2 (scaled-basis-blade-scale sbb2)])
                      (if (zero? w2)
                          0
                          (make-scaled-basis-blade
                           (* (canonical-reordering-sign b1 b2) w1 w2)
                           (fxior b1 b2))))))
              0))]
       [(number? sbb2)
        (let ([w1 (scaled-basis-blade-scale sbb1)])
          (scaled-basis-blade-with-scale sbb1 (* w1 sbb2)))]
       [else
        (bad-argument-types sbb1 sbb2)])]
     [(number? sbb1)
      (cond
       [(scaled-basis-blade? sbb2)
        (let ([w2 (scaled-basis-blade-scale sbb2)])
          (scaled-basis-blade-with-scale sbb2 (* w2 sbb1)))]
       [(number? sbb2)
        (* sbb1 sbb2)]
       [else
        (bad-argument-types sbb1 sbb2)])]
     [else
      (bad-argument-types sbb1 sbb2)]))

  ;; (canonical-reordering-sign fixnum1 fixnum2) → -1 or 1
  (define (canonical-reordering-sign b1 b2)
    (define (count-swaps b c)
      (if (fxzero? b)
          c
          (count-swaps (fxarithmetic-shift-right b 1)
                       (+ c (fxbit-count (fxand b b2))))))
    ;;
    ;; This assertion should be satisfied in any Scheme that has R⁶RS
    ;; fixnums, because a fixnum must have at least 24 bits.
    (assert (and (fixnum? b1) (fxpositive? b1)
                 (fixnum? b2) (fxpositive? b2)))
    ;;
    (- 1 (* 2 (mod (count-swaps (fxarithmetic-shift-right b1 1) 0)
                   2))))

  ) ;; end of library.
