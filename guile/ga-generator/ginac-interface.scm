;; Copyright (C) 2014 Barry Schwartz
;;
;; This file is part of Geometric-algebras.
;; 
;; Geometric-algebras is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3 of the
;; License, or (at your option) any later version.
;; 
;; Geometric-algebras is distributed in the hope that it will be
;; useful, but WITHOUT ANY WARRANTY; without even the implied warranty
;; of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (ga-generator ginac-interface)

  ;; A simple, non-object-oriented interface to GiNaC, supporting
  ;; almost nothing from that library but what is used in our GA
  ;; generator.
  
  (export

   GiNaC:make-symbol
   GiNaC:symbol?
   GiNaC:symbol-name
   GiNaC:symbol->string

   GiNaC:make-numeric
   GiNaC:numeric?
   GiNaC:numeric->string
   GiNaC:Digits

   GiNaC:make-ex
   GiNaC:ex?
   GiNaC:ex->string

   GiNaC:ex-nops
   GiNaC:ex-op

   GiNaC:ex-symbol?
   GiNaC:symbol->ex
   GiNaC:ex->symbol

   GiNaC:ex-numeric?
   GiNaC:numeric->ex
   GiNaC:ex->numeric

   GiNaC:ex-add?
   GiNaC:ex-add
   GiNaC:ex-sub

   GiNaC:ex-mul?
   GiNaC:ex-mul
   GiNaC:ex-div

   GiNaC:ex-power?
   GiNaC:ex-power

   GiNaC:ex-function?
   GiNaC:ex-function-name
   GiNaC:ex-abs

   GiNaC:ex-negate
   GiNaC:ex-expand

   GiNaC:ex=?
   GiNaC:ex<>?
   GiNaC:ex<?
   GiNaC:ex>?
   GiNaC:ex<=?
   GiNaC:ex>=?

   )

  (import (rnrs)
          (srfi srfi-9 gnu)
          (except (guile) error)
          (ice-9 format)
          (system foreign))

  (define-immutable-record-type GiNaC:symbol
    (GiNaC:make-symbol^^ pointer)
    GiNaC:symbol?
    (pointer GiNaC:symbol-pointer^^))

  (set-record-type-printer!
   GiNaC:symbol
   (lambda (obj port)
     (format port "#<GiNaC:symbol ~s 0x~x>"
             (GiNaC:symbol->string obj)
             (object-address obj))))

  (define-immutable-record-type GiNaC:numeric
    (GiNaC:make-numeric^^ pointer)
    GiNaC:numeric?
    (pointer GiNaC:numeric-pointer^^))

  (set-record-type-printer!
   GiNaC:numeric
   (lambda (obj port)
     (format port "#<GiNaC:numeric ~s 0x~x>"
             (GiNaC:numeric->string obj)
             (object-address obj))))

  (define-immutable-record-type GiNaC:ex
    (GiNaC:make-ex^^ pointer)
    GiNaC:ex?
    (pointer GiNaC:ex-pointer^^))

  (set-record-type-printer!
   GiNaC:ex
   (lambda (obj port)
     (format port "#<GiNaC:ex ~s 0x~x>"
             (GiNaC:ex->string obj)
             (object-address obj))))

  ;; We need these values to be in actual variables (rather than
  ;; written as syntax), so we can call them from C++.
  (define GiNaC:make-symbol^ GiNaC:make-symbol^^)
  (define GiNaC:symbol-pointer^ GiNaC:symbol-pointer^^)
  (define GiNaC:make-numeric^ GiNaC:make-numeric^^)
  (define GiNaC:numeric-pointer^ GiNaC:numeric-pointer^^)
  (define GiNaC:make-ex^ GiNaC:make-ex^^)
  (define GiNaC:ex-pointer^ GiNaC:ex-pointer^^)

  (eval-when (compile load eval)

    ;; Load the C++ part of the implementation of this module.

    (define (dynamic-link-ltdlopened lib)
      "If the C (or similar) library @var{lib} can be loaded
dynamically, load it. Otherwise assume it is already linked into the
program (possibly as a static library). Return the result of
@code{dynamic-link}."
      (catch #t
        (lambda () (dynamic-link lib))
        (lambda unused-args (dynamic-link))))

    (let* ([libguile-geometric-algebras-ginac
            (dynamic-link-ltdlopened
             "libguile-geometric-algebras-ginac")]
           [init_libguile_geometric_algebras_ginac
            (dynamic-func "init_libguile_geometric_algebras_ginac"
                          libguile-geometric-algebras-ginac)])
      (dynamic-call init_libguile_geometric_algebras_ginac
                    libguile-geometric-algebras-ginac)))

  ;; These two are synonyms.
  (define GiNaC:symbol->string GiNaC:symbol-name)

  (define GiNaC:Digits
    (make-procedure-with-setter Digits-ref Digits-set!))

  ) ;; end of library.
