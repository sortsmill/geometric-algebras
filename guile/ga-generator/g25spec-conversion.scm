;; Copyright (C) 2014 Barry Schwartz
;;
;; This file is part of Geometric-algebras.
;; 
;; Geometric-algebras is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3 of the
;; License, or (at your option) any later version.
;; 
;; Geometric-algebras is distributed in the hope that it will be
;; useful, but WITHOUT ANY WARRANTY; without even the implied warranty
;; of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (ga-generator g25spec-conversion)

  (export g25spec->ga-specification)

  (import (rnrs)
          (only (srfi :1) delete-duplicates list-index)
          (srfi :26)
          (srfi :42)
          (sxml match)
          (except (guile) error)
          (ice-9 match)
          (ice-9 format)
          (ga-generator gsl-interface)
          (ga-generator expression-parsers)
          (ga-generator scaled-basis-blades))

  ;;--------------------------------------------------------------------

  (define error-context (make-parameter #f))

  (define (g25spec-error message . irritants)
    (if (error-context)
        (apply error 'g25spec->ga-specification
               (format #f "~a: ~a" (error-context) message) irritants)
        (apply error 'g25spec->ga-specification message irritants)))

  (define (g25spec->ga-specification g25spec)
    "Read a <g25spec> XML file and produce an association list with
the parsed data."
    (define (make-spec node)
      (sxml-match node
        [(g25spec (@ (license ,license)
                     (language ,language)
                     (namespace ,namespace)
                     (coordStorage ,coordStorage)
                     (defaultOperatorBindings
                       (,defaultOperatorBindings "false"))
                     (dimension ,dimension)
                     (testSuite (,testSuite "false"))
                     (reportUsage (,reportUsage "false"))
                     (gmvCode (,gmvCode "expand"))
                     (parser (,parser "none"))
                     (copyright ,copyright))
                  . ,children)
         (let* ([dimension (posfixnum dimension "<g25spec dimension>")]
                [pass1-output
                 (cons* (cons 'license
                              (choice license '("custom" "gpl" "bsd")
                                      "<g25spec license>"))
                        (cons 'language
                              (choice language
                                      '("c"
                                        "cpp"
                                        "java"
                                        "csharp"
                                        "python"
                                        "matlab")
                                      "<g25spec language>"))
                        (cons 'namespace
                              (nonnull namespace "<g25spec namespace>"))
                        (cons 'coordinate-storage
                              (choice coordStorage '("array" "variables")
                                      "<g25spec coordStorage>"))
                        (cons 'default-operator-bindings
                              (t/f defaultOperatorBindings
                                   "<g25spec defaultOperatorBindings>"))
                        (cons 'dimension dimension)
                        (cons 'test-suite
                              (t/f testSuite "<g25spec testSuite>"))
                        (cons 'report-usage
                              (t/f reportUsage "<g25spec reportUsage>"))
                        (cons 'general-multivector-code
                              (choice gmvCode '("expand" "runtime")
                                      "<g25spec gmvCode>"))
                        (cons 'parser
                              (choice parser '("none" "builtin" "antlr")
                                      "<g25spec parser>"))
                        (cons 'copyright copyright)
                        (convert-g25spec-children-pass1 children
                                                        dimension))]
                [pass2-output
                 (convert-g25spec-children-pass2 pass1-output)]
                [pass3-output
                 (convert-g25spec-children-pass3 pass2-output)]
                [final-result pass3-output])
           final-result)]))
    (sxml-match g25spec
      [(*TOP* (*PI* . ,anything)
              (g25spec (@ . ,attributes) . ,children))
       (make-spec `(g25spec (@ . ,attributes) . ,children))]
      [(*TOP* (g25spec (@ . ,attributes) . ,children))
       (make-spec `(g25spec (@ . ,attributes) . ,children))]
      [(g25spec (@ . ,attributes) . ,children)
       (make-spec `(g25spec (@ . ,attributes) . ,children))]
      [,otherwise (g25spec-error "SXML is not a g25spec" g25spec)]))

  (define (convert-g25spec-children-pass1 children dimension)
    "Make a pass through the children of the <g25spec> node and
produce a preliminary association list."
    (append-ec (:list node children)
      (not (string? node))
      (sxml-match node
        [(customLicense ,text)
         (list (cons 'custom-license text))]
        [(outputDirectory (@ (path ,path))
                          . ,ignored)
         (list (cons 'output-directory path))]
        [(outputFilename (@ (defaultName ,defaultName)
                            (customName ,customName))
                         . ,ignored)
         (list
          (list 'output-file-name
                (cons 'default-name defaultName)
                (cons 'custom-name customName)))]
        [(inline (@ (constructors ,constructors)
                    (set ,set)
                    (assign ,assign)
                    (operators ,operators)
                    (functions ,functions))
                 . ,ignored)
         (list
          (cons 'inline-constructors
                (t/f constructors "<inline constructors>"))
          (cons 'inline-set (t/f set "<inline set>"))
          (cons 'inline-assign (t/f assign "<inline assign>"))
          (cons 'inline-operators (t/f operators "<inline operators>"))
          (cons 'inline-functions
                (t/f functions "<inline functions>")))]
        [(floatType (@ (type ,type)
                       (prefix (,prefix ""))
                       (suffix (,suffix "")))
                    . ,ignored)
         (list
          (list 'float-type
                (cons 'type type)
                (cons 'prefix prefix)
                (cons 'suffix suffix)))]
        [(basisVectorNames (@ . ,attributes)
                           . ,ignored)
         (list
          (cons 'basis-vector-names
                (convert-from-nameNN-attributes attributes
                                                dimension)))]
        [(metric (@ (name (,name "default"))
                    (round (,round "true")))
                 ,text)
         (list
          (list 'metric
                (cons 'name name)
                (cons 'round (t/f round "<metric round>"))
                (cons 'entries (string->metric-entry-list text))))]
        [(unaryOperator (@ (symbol ,symb)
                           (prefix ,prefix)
                           (function ,function))
                        . ,ignored)
         (list
          (list 'unary-operator
                (cons 'symbol symb)
                (cons 'prefix prefix)
                (cons 'function function)))]
        [(binaryOperator (@ (symbol ,symb)
                            (function ,function))
                         . ,ignored)
         (list
          (list 'binary-operator
                (cons 'symbol symb)
                (cons 'function function)))]
        [(mv (@ (name ,name)
                (compress ,compress)
                (coordinateOrder ,coordOrder)
                (memAlloc ,memAlloc))
             . ,basis-blades)
         (let ([coordinate-order
                (get-mv-coordinate-order coordOrder basis-blades)])
           (list
            (list 'mv
                  (cons 'name name)
                  (cons 'compress compress)
                  (cons 'coordinate-order coordinate-order)
                  (cons 'memory-allocation memAlloc)
                  (cons 'basis-blades basis-blades))))]
        [(smv (@ (name ,name)
                 (const (,const "false"))
                 (type ,type))
              . ,basis-blades)
         (list
          (list 'smv
                (cons 'name name)
                (cons 'constant (t/f const "<smv const>"))
                (cons 'type type)
                (cons 'basis-blades basis-blades)))]
        [(constant (@ (name ,name)
                      (type ,type))
                   . ,contents)
         (let-values ([(blades comment)
                       (get-text-and-comment contents)])
           (list
            (list 'constant
                  (cons 'name name)
                  (cons 'type type)
                  (cons 'basis-blades blades)
                  (cons 'comment comment))))]
        [(om (@ (name ,name)
                (coordinateOrder (,coordinateOrder "default")))
             . ,contents)
         (let-values ([(domain range) (get-domain-and-range contents)])
           (list
            (list 'om
                  (cons 'name name)
                  (cons 'coordinate-order
                        (choice coordinateOrder '("default" "custom")
                                "<om coordinateOrder>"))
                  (cons 'domain domain)
                  (cons 'range range))))]
        [(som (@ (name ,name)) . ,contents)
         (let-values ([(domain range) (get-domain-and-range contents)])
           (list
            (list 'som
                  (cons 'name name)
                  (cons 'domain domain)
                  (cons 'range range))))]
;;;;;;;                  (cons 'domain
;;;;;;;                        (get-basis-blades-without-assignments domain))
;;;;;;;                  (cons 'range
;;;;;;;                        (get-basis-blades-without-assignments range)))))]
        [(function (@ (name ,name)
                      (outputName (,outputName ""))
                      (returnType (,returnType ""))
                      (floatType (,floatType ""))
                      (metric (,metric "default"))
                      (comment (,comment ""))
                      . ,specially-handled-attributes)
                   . ,ignored)
         (let-values
             ([(args argnames options)
               (get-function-attrNN specially-handled-attributes)])
           (list
            (cons* 'function
                   (cons 'name name)
                   (cons 'output-name
                         (if (string-null? outputName) name outputName))
                   (cons 'return-type returnType)
                   (cons 'float-type floatType)
                   (cons 'metric metric)
                   (cons 'arguments args)
                   (cons 'argument-names argnames)
                   options)))]
        [(verbatim (@ (position ,position)
                      (marker (,marker ""))
                      (codeFilename (,codeFilename ""))
                      . ,filenameX-attributes)
                   ,text)
         (let ([file-names
                (get-file-names-for-verbatim filenameX-attributes)])
           (list
            (list 'verbatim
                  (cons 'file-names file-names)
                  (cons 'position position)
                  (cons 'marker marker)
                  (cons 'code-file-name codeFilename)
                  (cons 'text text))))]
        [,other
         (if (string? other)
             '()                        ; Ignore strings.
             (g25spec-error "unrecognized tag" other))])))

  (define (convert-g25spec-children-pass2 pass1-output)
    "Make a pass through the association list made by pass 1,
producing a new association."
    (list-ec (:list entry pass1-output)
      (match entry
        [('metric ('name . name)
                  ('round . round)
                  ('entries . entries))
         `(metric (name . ,name)
                  (round . ,round)
                  (entries
                   . ,(eval-metric-entries pass1-output entries)))]
        [('mv ('name . name)
              ('compress . compress)
              ('coordinate-order . coordinate-order)
              ('memory-allocation . memAlloc)
              ('basis-blades . basis-blades))
         `(mv (name . ,name)
              (compress . ,compress)
              (coordinate-order . ,coordinate-order)
              (memory-allocation . ,memAlloc)
              (grades . ,(get-mv-grades pass1-output
                                        coordinate-order
                                        basis-blades)))]
        [('smv ('name . name)
               ('constant . constant)
               ('type . type)
               ('basis-blades . basis-blades))
         `(smv (name . ,name)
               (constant . ,constant)
               (type . ,type)
               (basis-blades
                . ,(eval-smv-basis-blades
                    pass1-output constant
                    (string-join-nicely basis-blades))))]
        [('constant ('name . name)
                    ('type . type)
                    ('basis-blades . basis-blades)
                    ('comment . comment))
         `(constant (name . ,name)
                    (type . ,type)
                    (basis-blades
                     . ,(eval-constant-basis-blades pass1-output
                                                    basis-blades))
                    (comment . ,comment))]
        [('om ('name . name)
              ('coordinate-order . coordinateOrder)
              ('domain . domain)
              ('range . range))
         (let-values ([(domain^ range^)
                       (eval-om-domain-and-range pass1-output
                                                 coordinateOrder
                                                 domain range)])
           `(om (name . ,name)
                (coordinate-order . ,coordinateOrder)
                (domain . ,domain^)
                (range . ,range^)))]
        [('som ('name . name)
               ('domain . domain)
               ('range . range))
         (let-values ([(domain^ range^)
                       (eval-som-domain-and-range pass1-output
                                                  domain range)])
           `(som (name . ,name)
                 (domain . ,domain^)
                 (range . ,range^)))]
        [anything-else anything-else])))

  (define (convert-g25spec-children-pass3 pass2-output)
    "Make a pass through the association list made by pass 2,
producing a new association."
    (construct-metrics pass2-output))

  (define (construct-metrics spec)
    (let-values ([(metric-entries other-entries)
                  (partition (lambda (entry)
                               (eq? (car entry) 'metric))
                             spec)])
      (let* ([dimension (get-value spec 'dimension)]
             [metric-names
              (delete-duplicates (list-ec (:list entry metric-entries)
                                   (assq-ref (cdr entry) 'name)))]
             [grouped-by-name
              (list-ec (:list name metric-names)
                (cons name
                      (filter (lambda (entry)
                                (string=? (assq-ref (cdr entry) 'name)
                                          name))
                              metric-entries)))])
        (append
         (list-ec (:list group grouped-by-name)
           (match group
             [(group-name . group-entries)
              (let ([round-settings (list-ec (:list entry group-entries)
                                      (assq-ref entry 'round))]
                    [index-pairs (metric-index-pairs group-entries)])
                (unless (apply eq? round-settings)
                  (g25spec-error "inconsistent <metric round> settings"
                                 group-name))
                (unless (= (length (delete-duplicates index-pairs))
                           (length index-pairs))
                  (g25spec-error "duplicate <metric> entries"
                                 group-name))
                (let* ([diagonal? (metric-is-diagonal? index-pairs)]
                       [rounding?
                        ;; Rounding is forced off is the metric is
                        ;; diagonal.
                        (and (not diagonal?) (car round-settings))]
                       [rounding-epsilon 1e-14]
                       [matrix
                        (make-metric-matrix dimension group-entries)]
                       [euclidean?
                        (metric-is-euclidean? dimension diagonal?
                                              matrix)]
                       [anti-euclidean?
                        (metric-is-anti-euclidean? dimension diagonal?
                                                   matrix)]
                       [simple-diagonal?
                        (metric-is-simple-diagonal? dimension diagonal?
                                                    matrix)])
                  (let-values ([(eigenvalues eigenvectors)
                                (eigensystem dimension matrix
                                             diagonal? rounding?
                                             rounding-epsilon)])
                    (let ([positive-definite?
                           (metric-is-positive-definite? eigenvalues)]
                          [degenerate?
                           (metric-is-degenerate? eigenvalues)])
                      `(metric
                        (name . ,group-name)
                        (diagonal . ,diagonal?)
                        (round . ,rounding?)
                        (matrix . ,matrix)
                        ;;
                        ;; These eigenvalues of the metric form the
                        ;; ‘eigenmetric’.
                        (eigenvalues . ,eigenvalues)
                        ;;
                        ;; The columns of this matrix are eigenvectors
                        ;; of the metric. The matrix is orthonormal.
                        (eigenvectors . ,eigenvectors)
                        ;;
                        (euclidean . ,euclidean?)
                        (anti-euclidean . ,anti-euclidean?)
                        (simple-diagonal . ,simple-diagonal?)
                        (positive-definite . ,positive-definite?)
                        (degenerate . ,degenerate?) )))))]))
         other-entries))))

  (define (metric-is-diagonal? index-pairs)
    (for-all (lambda (pair)
               (fx=? (car pair) (cdr pair)))
             index-pairs))

  (define (metric-is-euclidean? dimension diagonal? matrix)
    (and diagonal?
         (for-all (lambda (i)
                    (= (array-ref matrix i i) 1))
                  (iota dimension))))

  (define (metric-is-anti-euclidean? dimension diagonal? matrix)
    (and diagonal?
         (for-all (lambda (i)
                    (= (array-ref matrix i i) -1))
                  (iota dimension))))

  (define (metric-is-simple-diagonal? dimension diagonal? matrix)
    (and diagonal?
         (for-all (lambda (i)
                    (let ([value (array-ref matrix i i)])
                      (or (zero? value)
                          (= value 1)
                          (= value -1))))
                  (iota dimension))))

  (define (metric-is-positive-definite? eigenvalues)
    ;; One also could use SRFI-43 vector-every, but Guile supports
    ;; SRFI-43 only since January of 2014.
    (for-all positive? (vector->list eigenvalues)))

  (define (metric-is-degenerate? eigenvalues)
    ;; One also could use SRFI-43 vector-any, but Guile supports
    ;; SRFI-43 only since January of 2014.
    (exists zero? (vector->list eigenvalues)))

  (define (eigensystem dimension matrix diagonal? rounding?
                       rounding-epsilon)
    (if diagonal?
        (let ([eigenvalues (vector-ec (:range i dimension)
                             (array-ref matrix i i))]
              [eigenvectors (make-array 0 dimension dimension)])
          (do-ec (:range i dimension)
            (array-set! eigenvectors 1 i i))
          (values eigenvalues eigenvectors))
        (let-values ([(eigenvalues eigenvectors)
                      (eigensystem-of-real-symmetric-matrix matrix)])
          (when rounding?
            ;; Round eigenmetric values that are near an integer.
            ;; (The ‘eigenmetric’ means the eigenvalues of the metric
            ;; matrix.)
            (do-ec (:range i dimension)
              (let* ([eigenval (vector-ref eigenvalues i)]
                     [rounded-val (round eigenval)])
                (when (< (abs (- rounded-val eigenval))
                         rounding-epsilon)
                  (vector-set! eigenvalues i rounded-val)))))
          (values eigenvalues eigenvectors))))

  (define (make-metric-matrix dimension metric-entries)
    "Return a metric matrix, which is represented as an array indexed
by row and column, in that order."
    (let ([matrix (make-array 0 dimension dimension)])
      (do-ec (:list metric-data metric-entries)
        (do-ec (:list cell-entry (assq-ref (cdr metric-data) 'entries))
          (match cell-entry
            [(index-pair . value)
             (let ([row-index (car index-pair)]
                   [column-index (cdr index-pair)])
               ;; Metric matrices are symmetric.
               (array-set! matrix value row-index column-index)
               (array-set! matrix value column-index row-index))])))
      matrix))

  (define (metric-index-pairs metric-entries)
    (apply append
           (list-ec (:list entry metric-entries)
             (let ([matrix-entries (assq-ref entry 'entries)])
               (map car matrix-entries)))))

  (define (eval-metric-entries pass1-output entries)
    "Evaluate <metric> tag entries from the first pass output,
producing pairs, whose cars are scaled basis vector pairs, and whose
cdrs are scalars."
    (let ([name->index (make--basis-vector-name->index pass1-output)])
      (list-ec (:list entry entries)
        (match entry
          [(basis-vectors inner-product-value)
           ;; Metric matrices are symmetric, so let us enforce a
           ;; canonical order of indices: put the lesser index ahead
           ;; of the greater.
           (let ([i1 (name->index (car basis-vectors))]
                 [i2 (name->index (cadr basis-vectors))])
             (cons (if (< i1 i2) (cons i1 i2) (cons i2 i1))
                   (eval-numeric-form inner-product-value)))]))))

  (define (get-mv-coordinate-order coordOrder basis-blades)
    "Determine, for the <mv> tag, whether the tag specifies default
coordinate order, or whether it specifies a custom order. If the
latter, determine whether the custom order is by grades or by
groups. Do some checks for correctness now, so procedures called later
can assume correctness."
    (match coordOrder
      ["default"
       (unless (string-list-is-whitespace? basis-blades)
         (g25spec-error
          "<mv coordOrder=\"default\"> but the tag has children"
          basis-blades))
       "default"]
      ["custom"
       (let ([bad-contents-error
              (lambda ()
                (g25spec-error
                 "<mv coordOrder=\"custom\"> but the basis blades are written incorrectly"
                 basis-blades))])
         (cond [(mv-groups-given? basis-blades)
                (unless (strings-in-list-are-whitespace? basis-blades)
                  (bad-contents-error))
                (let ([bb (filter (negate string?) basis-blades)])
                  (unless (group-list? bb)
                    (bad-contents-error)))
                "groups"]
               [else
                (unless (string-list? basis-blades)
                  (bad-contents-error))
                "grades"]))]
      [_
       (g25spec-error "<mv> has bad coordOrder" coordOrder)]))

  (define (mv-groups-given? coordOrder)
    "Are there any <group> tags nested within the <mv> tag?"
    (exists (lambda (e) (and (list? e)
                             (not (null? e))
                             (eq? (car e) 'group)))
            coordOrder))

  (define (get-mv-grades pass1-output coordinate-order basis-blades)
    "Analyze the <mv> tag. Produce a list whose entries represent
grades of basis blades. Each such entry is itself a list, whose
entries represent compression groups. Each group entry is a list of
scaled basis blades (including a scalar for the case of grade 0)."
    (let ([dimension (get-value pass1-output 'dimension)]
          [eval-basis-blade-expr
           (make--eval-basis-blade-expr pass1-output)]
          [do-final-check
           (lambda (dimension grades)
             (check-basis-blade-count
              dimension
              (apply append (apply append grades))
              "<mv coordOrder='custom'>")
             (check-basis-blade-bitmaps-unique
              (apply append (apply append grades))
              "<mv coordOrder='custom'>"))])
      (match coordinate-order
        ["default"
         (let ([bitmaps-by-grade
                (default-basis-blade-bitmaps-partitioned-by-grade
                  dimension)])
           (list-ec (:list grade bitmaps-by-grade)
             (list ;; Each grade is co-extensive with a group.
              (list-ec (:list bitmap grade)
                (make-scaled-basis-blade 1 bitmap)))))]
        ["grades"
         (let ([s-exprs (get-basis-blades-without-assignments
                         (string-join-nicely basis-blades)
                         "<mv coordOrder='custom'>")])
           (let ([grades
                  (map list ;; Each grade is co-extensive with a group.
                       (partition-scaled-basis-blades-by-grade
                        dimension
                        (map (lambda (e) (eval-basis-blade-expr (car e)))
                             s-exprs)))])
             (do-final-check dimension grades)
             grades))]
        ["groups"
         (let* ([groups
                 (list-ec (:list child basis-blades)
                   (and (pair? child) (eq? (car child) 'group))
                   (let* ([group (cdr child)]
                          [s-exprs (get-basis-blades-without-assignments
                                    (string-join-nicely group)
                                    "<mv coordOrder='custom'>")]
                          [blades
                           (map (lambda (e)
                                  (eval-basis-blade-expr (car e)))
                                s-exprs)])
                     (check-group-nonempty
                      blades "<mv coordOrder='custom'><group>")
                     (check-equal-grades
                      blades "<mv coordOrder='custom'><group>")                
                     blades))]
                [grades (partition-groups-by-grade dimension groups)])
           (do-final-check dimension grades)
           grades)])))

  (define* (check-basis-blade-count dimension blades #:optional context)
    "Check for the right number of basis blades."
    (if context
        (parameterize ([error-context context])
          (check-basis-blade-count dimension blades))
        (unless (= (length blades) (expt 2 dimension))
          (g25spec-error
           (format #f "expected ~a basis blades but got ~a"
                   (expt 2 dimension)
                   (length blades))
           blades))))

  (define* (check-group-nonempty blades #:optional context)
    "Check that a basis blade group is non-empty."
    (if context
        (parameterize ([error-context context])
          (check-group-nonempty blades))
        (when (null? blades)
          (g25spec-error "a basis blade group is empty"))))

  (define* (check-equal-grades blades #:optional context)
    "Check that all members of a basis blade group are of the same
grade."
    (if context
        (parameterize ([error-context context])
          (check-equal-grades blades))
        (let ([grades (list-ec (:list bb blades)
                        (scaled-basis-blade-grade bb))])
          (unless (apply = grades)
            (g25spec-error "basis blade group contains multiple grades"
                           blades)))))

  (define (partition-bitmaps-by-grade dimension bitmaps)
    "Partition a list of fixnums (which represent the bitmap fields of
scaled basis blades) into sublists, according to how many bits are set
in the respective fixnums."
    (let ([grades (make-vector (+ dimension 1) '())])
      (do-ec (:list bitmap bitmaps)
        (let ([bitcount (fxbit-count bitmap)])
          (vector-set! grades bitcount
                       (cons bitmap (vector-ref grades bitcount)))))
      (map reverse! (vector->list grades))))

  (define* (partition-scaled-basis-blades-by-grade
            dimension basis-blades #:optional [check-order? #t])
    "Partition a list of scaled basis blades (including scalars) into
sublists, according to the grades of the respective basis blades."
    (let ([grades (make-vector (+ dimension 1) '())])
      (do-ec (:list bb basis-blades)
        (let ([i (scaled-basis-blade-grade bb)])
          (vector-set! grades i (cons bb (vector-ref grades i)))))
      (let ([partition (map reverse! (vector->list grades))])
        (when (and check-order?
                   (not (equal? (apply append partition) basis-blades)))
          (g25spec-error "basis blades not in order of increasing grade"
                         basis-blades))
        partition)))

  (define* (partition-groups-by-grade dimension groups
                                      #:optional [check-order? #t])
    "Partition a list of basis blade groups into sublists, according
to the grades of the respective groups."
    (let ([grades (make-vector (+ dimension 1) '())])
      (do-ec (:list group groups)
        (let ([i (scaled-basis-blade-grade (car group))])
          (vector-set! grades i (cons group (vector-ref grades i)))))
      (let ([partition (map reverse! (vector->list grades))])
        (when (and check-order?
                   (not (equal? (apply append (apply append partition))
                                (apply append groups))))
          (g25spec-error
           "basis blade groups not in order of increasing grade"
           groups))
        partition)))

  (define (eval-smv-basis-blades pass1-output constant basis-blades)
    "Evaluate the basis blade expressions of an <smv> tag."
    ((if constant
         eval-smv-const-basis-blades
         eval-smv-nonconst-basis-blades)
     pass1-output basis-blades))

  (define (eval-smv-nonconst-basis-blades pass1-output basis-blades)
    "Evaluate the basis blade expressions of an <smv const='false'>
tag."
    (let ([entries (string->basis-blade-list basis-blades)]
          [eval-basis-blade-expr
           (make--eval-basis-blade-expr pass1-output)])
      (let ([blades
             (list-ec (:list entry entries)
               (match entry
                 [(blade value)
                  (cons (eval-basis-blade-expr blade)
                        (eval-numeric-form value))]
                 [(blade)
                  (cons (eval-basis-blade-expr blade) #f)]))])
        (check-basis-blade-bitmaps-unique (map car blades)
                                          error-context)
        blades)))

  (define (eval-smv-const-basis-blades pass1-output basis-blades)
    "Evaluate the basis blade expressions of an <smv const='true'>
tag."
    (eval-const-basis-blades pass1-output basis-blades
                             "<smv const='true'>"))

  (define (eval-constant-basis-blades pass1-output basis-blades)
    "Evaluate the basis blade expressions of a <constant> tag."
    (eval-const-basis-blades pass1-output basis-blades "<constant>"))

  (define (eval-const-basis-blades pass1-output basis-blades
                                   error-context)
    "Evaluate the basis blade expressions of a <constant> or <smv
const='true'> tag."
    (let ([assignments
           (get-basis-blades-with-mandatory-assignments basis-blades
                                                        error-context)]
          [eval-basis-blade-expr
           (make--eval-basis-blade-expr pass1-output)])
      (let ([blades
             (list-ec (:list entry assignments)
               (match entry
                 [(blade value)
                  (cons (eval-basis-blade-expr blade)
                        (eval-numeric-form value))]))])
        (check-basis-blade-bitmaps-unique (map car blades)
                                          error-context)
        blades)))

  (define (eval-om-domain-and-range pass1-output coordinateOrder
                                    domain range)
    (parameterize ([error-context "<om>"])
      (let ([dimension (get-value pass1-output 'dimension)]
            [domain-exprs (get-basis-blades-without-assignments domain)]
            [range-exprs (get-basis-blades-without-assignments range)])
        (match coordinateOrder
          ["default"
           (unless (null? domain-exprs)
             (g25spec-error
              "<om coordinateOrder='default'> has domain data given"
              domain))
           (unless (null? range-exprs)
             (g25spec-error
              "<om coordinateOrder='default'> has range data given"
              range))
           (let* ([bitmaps (default-basis-blade-bitmaps dimension)]
                  [blades (list-ec (:list bitmap bitmaps)
                            (make-scaled-basis-blade 1 bitmap))]
                  [domain^ blades]
                  [range^ blades])
             (values domain^ range^))]
          ["custom"
           (let* ([eval-basis-blade-expr
                   (make--eval-basis-blade-expr pass1-output)]
                  [domain^
                   (list-ec (:list entry domain-exprs)
                     (eval-basis-blade-expr (car entry)))]
                  [range^
                   (if (null? range-exprs)
                       domain^
                       (list-ec (:list entry range-exprs)
                         (eval-basis-blade-expr (car entry))))])
             (check-basis-blade-count
              dimension domain^ "<om coordOrder='custom'><domain>")
             (check-basis-blade-bitmaps-unique
              domain^ "<om coordOrder='custom'><domain>")
             (check-basis-blade-count
              dimension range^ "<om coordOrder='custom'><range>")
             (check-basis-blade-bitmaps-unique
              range^ "<om coordOrder='custom'><range>")
             (values domain^ range^))]))))

  (define (eval-som-domain-and-range pass1-output domain range)
    (parameterize ([error-context "<som>"])
      (let* ([domain-exprs (get-basis-blades-without-assignments domain)]
             [range-exprs (get-basis-blades-without-assignments range)]
             [eval-basis-blade-expr
              (make--eval-basis-blade-expr pass1-output)]
             [domain^
              (list-ec (:list entry domain-exprs)
                (eval-basis-blade-expr (car entry)))]
             [range^
              (if (null? range-exprs)
                  domain^
                  (list-ec (:list entry range-exprs)
                    (eval-basis-blade-expr (car entry))))])
        (check-basis-blade-bitmaps-unique domain^ "<som>")
        (check-basis-blade-bitmaps-unique range^ "<som>")
        (values domain^ range^))))

  (define (default-basis-blade-bitmaps dimension)
    (apply append
           (default-basis-blade-bitmaps-partitioned-by-grade
             dimension)))

  (define (default-basis-blade-bitmaps-partitioned-by-grade dimension)
    (partition-bitmaps-by-grade dimension (iota (expt 2 dimension))))

  (define* (check-basis-blade-bitmaps-unique blades #:optional context)
    (if context
        (parameterize ([error-context context])
          (check-basis-blade-bitmaps-unique blades))
        (let ([bitmaps (map scaled-basis-blade-bitmap blades)])
          (unless (= (length (delete-duplicates bitmaps =))
                     (length bitmaps))
            (g25spec-error "redundant blade entries" blades)))))

  (define* (get-basis-blades-without-assignments str #:optional context)
    (if context
        (parameterize ([error-context context])
          (get-basis-blades-without-assignments str))
        (let ([bblist (string->basis-blade-list str)])
          (do-not-allow-basis-blade-assignments bblist)
          bblist)))

  (define* (get-basis-blades-with-mandatory-assignments
            str #:optional context)
    (if context
        (parameterize ([error-context context])
          (get-basis-blades-with-mandatory-assignments str))
        (let ([bblist (string->basis-blade-list str)])
          (require-basis-blade-assignments bblist)
          bblist)))

  (define (do-not-allow-basis-blade-assignments bblist)
    (when (exists (lambda (e) (not (null? (cdr e)))) bblist)
      (g25spec-error
       "basis blade assignment in an <mv> definition"
       bblist)))

  (define (require-basis-blade-assignments bblist)
    (when (exists (lambda (e) (null? (cdr e))) bblist)
      (g25spec-error
       "basis blade without assignment in a <constant> definition"
       bblist)))

  (define (convert-from-nameNN-attributes attributes dimension)
    (list-ec (:range n 1 (+ dimension 1))
      (let* ([nameNN (string->symbol (format #f "name~a" n))]
             [pair (assq nameNN attributes)])
        (cadr pair))))

  (define (get-text-and-comment node-list)
    (let next-node ([lst node-list]
                    [text ""]
                    [comment ""])
      (if (null? lst)
          (values text comment)
          (sxml-match (car lst)
            [(comment ,more-comment)
             (next-node (cdr lst)
                        text
                        (string-extend comment more-comment))]
            [,other
             (if (string? (car lst))
                 (next-node (cdr lst)
                            (string-extend text (car lst))
                            comment)
                 (g25spec-error
                  "the only tags legal here are `(comment ...)'"
                  other))]))))

  (define (get-domain-and-range node-list)
    (let next-node ([lst node-list]
                    [domain ""]
                    [range ""])
      (if (null? lst)
          (values domain (if (string-null? range) domain range))
          (sxml-match (car lst)
            [(domain ,more-domain)
             (next-node (cdr lst)
                        (string-extend domain more-domain)
                        range)]
            [(range ,more-range)
             (next-node (cdr lst)
                        domain
                        (string-extend range more-range))]
            [,other
             (if (string? other)
                 (next-node (cdr lst) domain range)
                 (g25spec-error
                  "expected `(domain ...)' or `(range ...)'"
                  other))]))))

  (define (get-function-attrNN attributes)
    (let ([max-argNN (max-attrNN "arg" attributes)]
          [max-argNameNN (max-attrNN "argName" attributes)])
      (if (< max-argNN max-argNameNN)
          (g25spec-error (format #f
                                 "`argName~0@*~a' without `arg~0@*~a'"
                                 max-argNameNN))
          (let ([args (make-vector max-argNN "")]
                [argnames (make-vector max-argNN "")]
                [options '()])
            (let next-attr ([lst attributes])
              (if (null? lst)
                  (values (vector->list args)
                          (vector->list argnames)
                          (reverse options))
                  (begin
                    (match (car lst)
                      [((? argNN? key) arg-type)
                       (vector-set! args (attrNN->index key) arg-type)]
                      [((? argNameNN? key) arg-name)
                       (vector-set! argnames (attrNN->index key)
                                    arg-name)]
                      [((? optionXX? key) option-value)
                       (set! options (cons (cons key option-value)
                                           options))]
                      [other
                       (g25spec-error
                        "unrecognized <function> attribute" other)])
                    (next-attr (cdr lst)))))))))

  (define ascii-digit
    (char-set-intersection char-set:digit char-set:ascii))

  (define (argNN? symb)
    (attrNN? "arg" symb))

  (define (argNameNN? symb)
    (attrNN? "argName" symb))

  (define (attrNN? attr-prefix symb)
    (let ([s (symbol->string symb)])
      (and (string-prefix? attr-prefix s)
           (let ([t (string-drop s (string-length attr-prefix))])
             (and (not (string-null? t))
                  (string-every ascii-digit t))))))

  (define (attrNN->NN symb)
    (let* ([s (symbol->string symb)]
           [i^ (string-skip-right s ascii-digit)]
           [i (if i^ (+ i^ 1) 0)])
      (string-drop s i)))

  (define (attrNN->index symb)
    (- (string->number (attrNN->NN symb)) 1))

  (define (max-attrNN attr-prefix attributes)
    (fold-left (lambda (prior-max attr)
                 (if (attrNN? attr-prefix (car attr))
                     (max (string->number (attrNN->NN (car attr)))
                          prior-max)
                     prior-max))
               0 attributes))

  (define (optionXX? symb)
    (string-prefix? "option" (symbol->string symb)))

  (define (get-file-names-for-verbatim attributes)
    (let next-attr ([lst attributes]
                    [file-names '()])
      (if (null? lst)
          (reverse! file-names)
          (if (string-prefix? "filename" (symbol->string (caar lst)))
              (next-attr (cdr lst) (cons (cadar lst) file-names))
              (g25spec-error "unrecognized <verbatim> attribute key"
                             (caar lst))))))

  ;;--------------------------------------------------------------------

  (define (make--eval-basis-blade-expr spec)
    (let ([dimension (get-value spec 'dimension)]
          [basis-vector-name->basis-blade
           (make--basis-vector-name->scaled-basis-blade spec)]
          [reverse-sign (cut scaled-basis-blade-op -1 <>)])
      (cut eval-basis-blade-expression <>
           #:basis-blade-reverse-sign reverse-sign
           #:basis-blade-op scaled-basis-blade-op
           #:basis-vector-name->basis-blade
           basis-vector-name->basis-blade)))

  (define (make--basis-vector-name->scaled-basis-blade spec)
    "Return a @code{basis-vector-name->scaled-basis-blade} procedure."
    (let ([name->index (make--basis-vector-name->index spec)])
      (lambda (name)
        (if (string=? name "scalar")
            1
            (let* ([i (name->index name)]
                   [bitmap (fxarithmetic-shift-left 1 i)])
              (make-scaled-basis-blade 1 bitmap))))))

  (define (make--basis-vector-name->index spec)
    "Return a @code{basis-vector-name->index} procedure."
    (let ([names (basis-vector-names spec)])
      (lambda (name)
        (unless (string? name)
          (assertion-violation
           #f "expected a string (representing a basis vector name)"
           name names))
        (let ([i (list-index (cut string=? name <>) names)])
          (unless i
            (error #f "not a basis vector name" name names))
          i))))

;;;;  (define (make--index->basis-vector-name spec)
;;;;    "Return an @code{index->basis-vector-name} procedure."
;;;;    (let* ([names (basis-vector-names spec)]
;;;;           [dimension (length names)])
;;;;      (lambda (i)
;;;;        (unless (and (fixnum? i) (fx<? -1 i dimension))
;;;;          (assertion-violation
;;;;           #f
;;;;           (format #f "expected a fixnum in [0,~a]" (fx- dimension 1))
;;;;           i))
;;;;        (list-ref names i))))

  (define (basis-vector-names spec)
    "Return the list of basis vector names."
    (let ([names (get-value spec 'basis-vector-names)]
          [dimension (get-value spec 'dimension)])
      (check-basis-vector-names names dimension)
      names))

  (define (check-basis-vector-names names dimension)
    (check-basis-vector-names-type names)
    (check-basis-vector-names-count names dimension)
    (check-basis-vector-names-are-unique names))

  (define (check-basis-vector-names-type names)
    (unless (and (list? names)
                 (for-all string? names))
      (g25spec-error
       "basis-vector-names must be a list of strings"
       names)))

  (define (check-basis-vector-names-count names dimension)
    (unless (= (length names) dimension)
      (g25spec-error
       "the list of basis-vector-names does not match the dimension"
       names dimension)))

  (define (check-basis-vector-names-are-unique names)
    (unless (= (length names) (length (delete-duplicates names)))
      (g25spec-error
       "the basis-vector-names must not contain duplicates"
       names)))
  
  (define (get-value spec key)
    (let ([pair (assq key spec)])
      (unless pair
        (g25spec-error (format #f "`~a' data not found" key)))
      (cdr pair)))

  ;;--------------------------------------------------------------------

  (define whitespace
    ;; Anything that does not put ink on paper.
    (char-set-complement char-set:graphic))

  (define (string-is-whitespace? s)
    "Is the string entirely whitespace?"
    (string-every whitespace s))

  (define (string-list-is-whitespace? lst)
    "Is this a list, is every member a string, and is every one of the
strings entirely whitespace?"
    (and (list? lst)
         (for-all string? lst)
         (for-all string-is-whitespace? lst)))

  (define (strings-in-list-are-whitespace? lst)
    "Is this a list, and is every member of it that is a string
entirely whitespace?"
    (and (list? lst)
         (for-all string-is-whitespace? (filter string? lst))))

  (define (string-list? lst)
    "Is this a list, and are all the entries strings?"
    (and (list? lst) (for-all string? lst)))

  (define (group-list? lst)
    "Is this a list, and is every entry a <group> tag?"
    (and (list? lst)
         (for-all pair? lst)
         (for-all (lambda (e) (eq? (car e) 'group)) lst)
         (for-all (lambda (e) (string-list? (cdr e))) lst)
         (for-all (lambda (e) (not (string-list-is-whitespace? e)))
                  lst)))

  (define (string-join-nicely lst)
    (apply string-extend lst))

  (define (string-extend . lst)
    (match lst
      [() ""]
      [(t) t]
      [(s t) (if (string-null? s) t (string-join (list s t)))]
      [(s t . more) (apply string-extend (string-extend s t) more)]))

  (define* (t/f s #:optional context)
    (if context
        (parameterize ([error-context context])
          (t/f s))
        (match s
          ["true" #t]
          ["false" #f]
          [_ (g25spec-error "expected 'true' or 'false'" s)])))

  (define* (choice s choices #:optional context)
    (if context
        (parameterize ([error-context context])
          (choice s choices)
          (if (member s choices)
              s
              (g25spec-error "unexpected value" s choices)))))

  (define* (nonnull s #:optional context)
    (if context
        (parameterize ([error-context context])
          (nonnull s))
        (if (string-null? s)
            (g25spec-error "empty value" s)
            s)))

  (define* (posfixnum s #:optional context)
    (define (it-is-not-correct)
      (g25spec-error
       "expected a string that represents a positive fixnum" s))
    (if context
        (parameterize ([error-context context])
          (posfixnum s))
        (let ([n (catch #t
                   (lambda () (string->number s))
                   (lambda _ (it-is-not-correct)))])
          (if (and (fixnum? n) (positive? n))
              n
              (it-is-not-correct)))))

  (define (eval-numeric-form form)
    ;; FIXME: It would be better to evaluate in extra precision.
    (eval form (current-module)))

  ;;--------------------------------------------------------------------

  ) ;; end of library.
