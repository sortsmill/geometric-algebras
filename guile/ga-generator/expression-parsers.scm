;; Copyright (C) 2014 Barry Schwartz
;;
;; This file is part of Geometric-algebras.
;; 
;; Geometric-algebras is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3 of the
;; License, or (at your option) any later version.
;; 
;; Geometric-algebras is distributed in the hope that it will be
;; useful, but WITHOUT ANY WARRANTY; without even the implied warranty
;; of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (ga-generator expression-parsers)

  ;;
  ;; Conversion of Gaigen 2.5 expressions to s-expressions, plus means
  ;; to evaluate basis blade s-expressions.
  ;;

  (export string->metric-entry-list
          string->basis-blade-list
          eval-basis-blade-expression)

  (import (rnrs)
          (system base lalr) ;; lalr-scm <https://code.google.com/p/lalr-scm/>
          (except (guile) error)
          (ice-9 match)
          (ga-generator expression-lexer))

  ;;--------------------------------------------------------------------


  (define (string->metric-entry-list str)
    (let ([me-lexer (make-me-lexer str)]
          [me-error-handler
           (lambda (message . rest)
             (apply error 'parse-basis-blade-list message rest))]
          [me-parser (make-me-parser)])
      (me-parser me-lexer me-error-handler)))

  (define make-me-lexer
    (expression-lexer-maker
     '[LPAREN "("]
     '[RPAREN ")"]
     '[PERIOD "."]
     '= '+ '- '* '/ '**))

  (define (make-me-parser)
    (lalr-parser
     (IDENTIFIER
      NUMERAL
      LPAREN RPAREN
      (right: =)
      (left: - +)
      (left: * / PERIOD)
      (right: **)
      (nonassoc: uminus uplus))

     ;; melist: metric entry assignment list
     (melist ()                         : (list)
             (melist assign)            : (append $1 $2))

     ;; assign: one or more metric entries with an assignment
     (assign (me = numval)              : (list (list $1 $3))
             (me = assign)              : (cons (list $1 (cadar $3)) $3))

     ;; me: metric entry
     (me     (IDENTIFIER
              PERIOD
              IDENTIFIER)               : (list $1 $3))

     ;; numval: numeric value
     (numval (NUMERAL)                  : (list 'string->number $1)
             (LPAREN numval RPAREN)     : $2
             (numval + numval)          : (+ $1 $3)
             (numval - numval)          : (list '- $1 $3)
             (numval * numval)          : (list '* $1 $3)
             (numval / numval)          : (list '/ $1 $3)
             (numval ** numval)         : (list 'expt $1 $3)
             (- numval (prec: uminus))  : (reverse-sign $2)
             (+ numval (prec: uplus))   : $2)
     ))

  ;;--------------------------------------------------------------------

  (define (string->basis-blade-list str)
    (let ([bb-lexer (make-bb-lexer str)]
          [bb-error-handler
           (lambda (message . rest)
             (apply error 'parse-basis-blade-list message rest))]
          [bb-parser (make-bb-parser)])
      (bb-parser bb-lexer bb-error-handler)))

  (define make-bb-lexer
    (expression-lexer-maker
     '[LPAREN "("]
     '[RPAREN ")"]
     '= '^ '+ '- '* '/ '**))

  (define (make-bb-parser)
    (lalr-parser
     (IDENTIFIER
      NUMERAL
      LPAREN RPAREN
      (right: =)
      (left: - +)
      (left: * / ^)
      (right: **)
      (nonassoc: uminus uplus))

     ;; bblist: basis blade list
     (bblist ()                         : (list)
             (bblist bb)                : (append $1 (list (list $2)))
             (bblist assign)            : (append $1 $2))

     ;; assign: one or more basis blades with an assignment
     (assign (bb = numval)              : (list (list $1 $3))
             (bb = assign)              : (cons (list $1 (cadar $3)) $3))

     ;; bb: basis blade
     (bb     (IDENTIFIER)               : (list 'basis-vector-name->basis-blade $1)
             (bb ^ bb)                  : (list 'basis-blade-op $1 $3)
             (- bb (prec: uminus))      : (reverse-sign $2)
             (+ bb (prec: uplus))       : $2)

     ;; numval: numeric value
     (numval (NUMERAL)                  : (list 'string->number $1)
             (LPAREN numval RPAREN)     : $2
             (numval + numval)          : (+ $1 $3)
             (numval - numval)          : (list '- $1 $3)
             (numval * numval)          : (list '* $1 $3)
             (numval / numval)          : (list '/ $1 $3)
             (numval ** numval)         : (list 'expt $1 $3)
             (- numval (prec: uminus))  : (reverse-sign $2)
             (+ numval (prec: uplus))   : $2)
     ))

  (define* (eval-basis-blade-expression expr #:key
                                        basis-blade-reverse-sign
                                        basis-blade-op
                                        basis-vector-name->basis-blade)
    (define (eval-expr e)
      (match e
        [('- e1)
         (basis-blade-reverse-sign (eval-expr e1))]
        [('basis-blade-op e1 e2)
         (basis-blade-op (eval-expr e1) (eval-expr e2))]
        [('basis-vector-name->basis-blade (? string? e1))
         (basis-vector-name->basis-blade e1)]
        [_ (assertion-violation 'eval-basis-blade-expression
                                "not a valid expression"
                                expr)]))
    (eval-expr expr))

  ;;--------------------------------------------------------------------

  (define (reverse-sign value)
    (match value
      [('string->number numeral)
       (list 'string->number (reverse-numeral-sign numeral))]
      [('- v) v]
      [v (list '- v)]))

  (define (reverse-numeral-sign numeral)
    (if (char=? (string-ref numeral 0) #\-)
        (string-drop numeral 1)
        (string-append "-" numeral)))

  ;;--------------------------------------------------------------------

  ) ;; end of library.
