;; Copyright (C) 2014 Barry Schwartz
;;
;; This file is part of Geometric-algebras.
;; 
;; Geometric-algebras is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3 of the
;; License, or (at your option) any later version.
;; 
;; Geometric-algebras is distributed in the hope that it will be
;; useful, but WITHOUT ANY WARRANTY; without even the implied warranty
;; of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (ga-generator gsl-interface)

  ;; An interface to GSL, only for those things we need in our GA
  ;; generator.

  (export eigensystem-of-real-symmetric-matrix)

  (import (rnrs)
          (srfi :42)
          (except (guile) error)
          (system foreign)
          (ice-9 format))

  (define (dynamic-link-ltdlopened lib)
    "If the C (or similar) library @var{lib} can be loaded
dynamically, load it. Otherwise assume it is already linked into the
program (possibly as a static library). Return the result of
@code{dynamic-link}."
    (catch #t
      (lambda () (dynamic-link lib))
      (lambda unused-args (dynamic-link))))

  (define libgsl (dynamic-link-ltdlopened "libgsl"))

  (define gsl_matrix_alloc
    (dynamic-func "gsl_matrix_alloc" libgsl))
  (define gsl_matrix_free
    (dynamic-func "gsl_matrix_free" libgsl))
  (define gsl_matrix_get
    (dynamic-func "gsl_matrix_get" libgsl))
  (define gsl_matrix_set
    (dynamic-func "gsl_matrix_set" libgsl))

  (define gsl_vector_alloc
    (dynamic-func "gsl_vector_alloc" libgsl))
  (define gsl_vector_free
    (dynamic-func "gsl_vector_free" libgsl))
  (define gsl_vector_get
    (dynamic-func "gsl_vector_get" libgsl))
  
  (define gsl_eigen_symmv_alloc
    (dynamic-func "gsl_eigen_symmv_alloc" libgsl))
  (define gsl_eigen_symmv_free
    (dynamic-func "gsl_eigen_symmv_free" libgsl))
  (define gsl_eigen_symmv
    (dynamic-func "gsl_eigen_symmv" libgsl))

  (define (gsl-error message)
    (error #f message))

  (define parse-gsl-matrix
    ;;
    ;;  typedef struct
    ;;  {
    ;;    size_t size1;
    ;;    size_t size2;
    ;;    size_t tda;
    ;;    double *data;
    ;;    gsl_block *block;
    ;;    int owner;
    ;;  } gsl_matrix;
    ;;
    (let ([fields `(,size_t ,size_t ,size_t * * ,int)])
      (lambda (matrix)
        (parse-c-struct matrix fields))))

  (define (gsl-matrix-dimensions matrix)
    (let ([struct (parse-gsl-matrix matrix)])
      (values (car struct) (cadr struct))))

  (define gsl-matrix-alloc
    (let ([proc (pointer->procedure '* gsl_matrix_alloc
                                    `(,size_t ,size_t))])
      (lambda (m n)
        (let ([matrix (proc m n)])
          (when (null-pointer? matrix)
            (gsl-error "failure when allocating a GSL matrix"))
          matrix))))

  (define gsl-matrix-free
    (pointer->procedure void gsl_matrix_free '(*)))

  (define gsl-matrix-set!
    (pointer->procedure void gsl_matrix_set `(* ,size_t ,size_t ,double)))

  (define gsl-matrix-get
    (pointer->procedure double gsl_matrix_get `(* ,size_t ,size_t)))

  (define (array->gsl-matrix arr)
    ;; DOES NOT FREE THE RETURNED VALUE! The return value must be
    ;; freed with gsl-matrix-free.
    (let ([shape (array-shape arr)])
      (let ([imin (caar shape)]
            [imax (cadar shape)]
            [jmin (caadr shape)]
            [jmax (cadadr shape)])
        (let ([matrix (gsl-matrix-alloc (- imax imin -1)
                                        (- jmax jmin -1))])
          (do-ec (:range i imin (+ imax 1))
            (do-ec (:range j jmin (+ jmax 1))
              (gsl-matrix-set! matrix (- i imin) (- j jmin)
                               (array-ref arr i j))))
          matrix))))

  (define (gsl-matrix->array matrix)
    (let-values ([(m n) (gsl-matrix-dimensions matrix)])
      (let ([arr (make-array *unspecified* m n)])
        (do-ec (:range i m)
          (do-ec (:range j n)
            (array-set! arr (gsl-matrix-get matrix i j) i j)))
        arr)))

  (define parse-gsl-vector
    ;;
    ;;  typedef struct
    ;;  {
    ;;    size_t size;
    ;;    size_t stride;
    ;;    double *data;
    ;;    gsl_block *block;
    ;;    int owner;
    ;;  } gsl_vector;
    ;;
    (let ([fields `(,size_t ,size_t * * ,int)])
      (lambda (vec)
        (parse-c-struct vec fields))))

  (define (gsl-vector-dimension vec)
    (let ([struct (parse-gsl-vector vec)])
      (car struct)))

  (define gsl-vector-alloc
    (let ([proc (pointer->procedure '* gsl_vector_alloc `(,size_t))])
      (lambda (n)
        (let ([vec (proc n)])
          (when (null-pointer? vec)
            (gsl-error "failure when allocating a GSL vector"))
          vec))))

  (define gsl-vector-free
    (pointer->procedure void gsl_vector_free '(*)))

  (define gsl-vector-get
    (pointer->procedure double gsl_vector_get `(* ,size_t)))

  (define (gsl-vector->vector vec)
    (let* ([n (gsl-vector-dimension vec)]
           [v (make-vector n)])
      (do-ec (:range i n)
        (vector-set! v i (gsl-vector-get vec i)))
      v))

  (define gsl-eigen-symmv-alloc
    (let ([proc (pointer->procedure '* gsl_eigen_symmv_alloc
                                    `(,size_t))])
      (lambda (n)
        (let ([workspace (proc n)])
          (when (null-pointer? workspace)
            (gsl-error
             "failure when allocating GSL workspace (gsl_eigen_symmv_alloc)"))
          workspace))))

  (define gsl-eigen-symmv-free
    (pointer->procedure void gsl_eigen_symmv_free '(*)))

  (define gsl-eigen-symmv
    (pointer->procedure int gsl_eigen_symmv '(* * * *)))

  (define-syntax let/free
    (syntax-rules ()
      [(_ ([var value free]) body body* ...)
       (let ([var value])
         (dynamic-wind
           (lambda () *unspecified*)
           (lambda () body body* ...)
           (lambda () (free var))))]))

  (define (eigensystem-of-real-symmetric-matrix arr)
    (let/free
     ([A (array->gsl-matrix arr) gsl-matrix-free])
     (let-values ([(m n) (gsl-matrix-dimensions A)])
       (assert (= m n))
       (let/free
        ([eigenvalues (gsl-vector-alloc m) gsl-vector-free])
        (let/free
         ([eigenvectors (gsl-matrix-alloc m n) gsl-matrix-free])
         (let/free
          ([workspace (gsl-eigen-symmv-alloc m) gsl-eigen-symmv-free])
          (let ([status (gsl-eigen-symmv A eigenvalues eigenvectors
                                         workspace)])
            (unless (zero? status)
              (gsl-error "GSL error while running gsl_eigen_symmv"))
            (values (gsl-vector->vector eigenvalues)
                    (gsl-matrix->array eigenvectors)))))))))

  ) ;; end of library.
