;; Copyright (C) 2014 Barry Schwartz
;;
;; This file is part of Geometric-algebras.
;; 
;; Geometric-algebras is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3 of the
;; License, or (at your option) any later version.
;; 
;; Geometric-algebras is distributed in the hope that it will be
;; useful, but WITHOUT ANY WARRANTY; without even the implied warranty
;; of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (ga-generator expression-lexer)

  ;;
  ;;  Example:
  ;;
  ;;    (define make-lexer
  ;;      (expression-lexer-maker '(LPAREN "(") '(RPAREN ")") '+ '- 'div))
  ;;    (define lexer (make-lexer "-a - 3.4 + (b div c)"))
  ;;
  ;;    (let loop () ; Demonstrate the lexer.
  ;;      (let ([x (lexer)])
  ;;        (unless (eq? x '*eoi*)
  ;;          (format #t "~s\n" x)
  ;;          (loop))))
  ;;

  (export expression-lexer-maker)

  (import (rnrs)
          (srfi :42)
          (system base lalr) ;; lalr-scm <https://code.google.com/p/lalr-scm/>
          (except (guile) error)
          (ice-9 format)
          (ice-9 match))

  (define whitespace
    ;; Anything that does not put ink on paper.
    (char-set-complement char-set:graphic))

  (define ident-starters
    (char-set-union char-set:letter (->char-set "_")))

  (define ident-parts
    (char-set-union char-set:letter+digit (->char-set "_")))

  (define exponent-part-starters (->char-set "e"))

  (define plus+minus (->char-set "-+"))

  (define (expression-lexer-maker . symbol-list)
    (let ([symbol-strings
           (in-descending-lengths
            (list-ec (:list symb symbol-list)
              (match symb
                [(? symbol? symb) (list (symbol->string symb) symb)]
                [((? symbol? symb) (? string? str)) (list str symb)]
                [_ (assertion-violation 'make-lexer-maker
                                        "bad specification"
                                        symb)])))])
      (lambda* (text #:optional [start 0] end) ;; The lexer maker.
        (let* ([start 0]
               [end (or end (string-length text))]
               [s text])
          (lambda () ;; The lexer.
            (let ([i (string-skip s whitespace start end)])
              (if (not i)
                  '*eoi*
                  (cond
                   [(char-set-contains? ident-starters (string-ref s i))
                    ;; Either an identifier or an identifier-like
                    ;; symbol.
                    (let* ([j (or (string-skip s ident-parts i end) end)]
                           [ident (substring s i j)])
                      (set! start j)
                      (cond
                       [(assoc ident symbol-strings) =>
                        ;; An identifier-like symbol.
                        (lambda (string-and-symb)
                          (make-lexical-token
                           (cadr string-and-symb)
                           (make-source-location s #f #f i (- j i))
                           *unspecified*))]
                       [else
                        ;; An identifier.
                        (make-lexical-token
                         'IDENTIFIER
                         (make-source-location s #f #f i (- j i))
                         ident)]))]
                   [(operator-is-prefix? symbol-strings s i end) =>
                    ;; A non-identifier-like symbol.
                    (lambda (string-and-symb)
                      (let ([token-length
                             (string-length (car string-and-symb))])
                        (set! start (+ i token-length))
                        (make-lexical-token
                         (cadr string-and-symb)
                         (make-source-location s #f #f i token-length)
                         *unspecified*)))]
                   [(unsigned-numeral-is-prefix? s i end) =>
                    ;; A numeral.
                    (lambda (numeral)
                      (let ([len (string-length numeral)])
                        (set! start (+ i len))
                        (make-lexical-token
                         'NUMERAL
                         (make-source-location s #f #f i len)
                         numeral)))]
                   [else (error 'make-lexer-maker
                                "lexical error"
                                i text)]))))))))

  (define (operator-is-prefix? symbol-strings s start end)
    (and (not (null? symbol-strings))
         (let ([symb-str (caar symbol-strings)])
           (if (string-prefix? symb-str s
                               0 (string-length symb-str)
                               start end)
               (car symbol-strings)
               (operator-is-prefix? (cdr symbol-strings) s
                                    start end)))))

  (define (unsigned-numeral-is-prefix? s start end)
    (cond [(<= end start) #f]
          [(run-of-digits-is-prefix? s start end) =>
           (lambda (run)
             (let ([after-run (+ start (string-length run))])
               (cond
                [(<= end after-run) run]
                [(char=? (string-ref s after-run) #\.)
                 (let ([after-period (+ after-run 1)])
                   (cond
                    [(exponent-part-is-prefix? s after-period end) =>
                     (lambda (expon)
                       (string-append run "." expon))]
                    [(run-of-digits-is-prefix? s after-period end) =>
                     (lambda (run2)
                       (let ([after-run2
                              (+ after-period (string-length run2))])
                         (cond
                          [(exponent-part-is-prefix? s after-run2 end) =>
                           (lambda (expon)
                             (string-append run "." run2 expon))]
                          [else
                           (string-append run "." run2)])))]
                    [else
                     (string-append run ".")]))]
                [(exponent-part-is-prefix? s after-run end) =>
                 (lambda (expon)
                   (string-append run expon))]
                [else run])))]
          [(char=? (string-ref s start) #\.)
           (let ([after-period (+ start 1)])
             (cond
              [(run-of-digits-is-prefix? s after-period end) =>
               (lambda (run2)
                 (let ([after-run2 (+ after-period (string-length run2))])
                   (cond
                    [(exponent-part-is-prefix? s after-run2 end) =>
                     (lambda (expon)
                       (string-append "." run2 expon))]
                    [else
                     (string-append "." run2)])))]
              [else #f]))]
          [else #f]))

  (define (exponent-part-is-prefix? s start end)
    (cond
     [(<= end start) #f]
     [(char-set-contains? exponent-part-starters (string-ref s start))
      (let ([after-e (+ start 1)])
        (cond [(<= end after-e) #f]
              [(run-of-digits-is-prefix? s after-e end) =>
               (lambda (run3)
                 (string-append (substring s start after-e) run3))]
              [(char-set-contains? plus+minus (string-ref s after-e))
               (let* ([after-sign (+ after-e 1)]
                      [run3 (run-of-digits-is-prefix? s after-sign end)])
                 (and run3
                      (string-append (substring s start after-sign)
                                     run3)))]
              [else #f]))]
     [else #f]))

  (define (run-of-digits-is-prefix? s start end)
    (cond [(<= end start) #f]
          [(char-set-contains? char-set:digit (string-ref s start))
           (let ([i-nondigit
                  (or (string-skip s char-set:digit start end) end)])
             (substring s start i-nondigit))]
          [else #f]))

  (define (in-descending-lengths strings)
    (list-sort (lambda (a b)
                 (< (string-length (car b)) (string-length (car a))))
               strings))

  ) ;; end of library.
