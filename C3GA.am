# -*- coding: utf-8; tab-width: 4 -*-
#
# Copyright (C) 2014 Barry Schwartz
#
# This file is part of Geometric-algebras.
# 
# Geometric-algebras is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3 of the
# License, or (at your option) any later version.
# 
# Geometric-algebras is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.

.PHONY: update-C3GA
update-all-GAs: update-C3GA
update-C3GA: C3GA-after-renamings.xml; $(call v, RUNNING)$(call update_from_xml,C3GA)
MOSTLYCLEANFILES += $(patsubst %.xml,%-after-renamings.xml,C3GA)

geometric_algebras_guile_@GEV@_LDADD += libC3GA-guile-@GEV@.la

lib_LTLIBRARIES += libC3GA.la
libC3GA_la_SOURCES = C3GA.c C3GA_parse_C3GA_mv.c
inc_geometric_algebras_HEADERS += C3GA.h
libC3GA_la_CFLAGS = $(AM_CFLAGS) $(G25_WARNING_CFLAGS)
libC3GA_la_LDFLAGS = -export-dynamic -prefer-pic
libC3GA_la_LIBADD = lib/libgnu.la

guilelib_LTLIBRARIES += libC3GA-guile-@GEV@.la
libC3GA_guile_@GEV@_la_SOURCES = C3GA_guile.c
libC3GA_guile_@GEV@_la_CFLAGS = $(AM_CFLAGS) $(CFLAG_VISIBILITY)
libC3GA_guile_@GEV@_la_LDFLAGS = -export-dynamic -prefer-pic
libC3GA_guile_@GEV@_la_LIBADD = lib/libgnu.la libC3GA.la

MY_DLOPENLIBS += C3GA-guile-@GEV@ C3GA

NODIST_GUILESRCMODULES += guile/geometric-algebras/C3GA.scm
NODIST_GUILESRCMODULES += guile/geometric-algebras/C3GA/base.scm

guile/geometric-algebras/C3GA.go: guile/geometric-algebras/C3GA/base.go
